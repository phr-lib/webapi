<?php

namespace Phr\Webapi;

use Phr\Webapi\ApiTools\CoreService;
use Phr\Webapi\ApiTools\Entity;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiControl\ResponseCode as RC;


class Service extends CoreService
{
    public function insert(object $entity)
    {     
        try{
            $result = self::$sql->query($entity->insert(Entity::getTable(get_class($entity))));
            
        }catch(SqlException $error)
        {   
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604010);
        }
    }
    public function fetch(string $entity, array|null $_where = null)
    {     
        try{
            return self::$sql->fetch(Entity::getTable($entity), $_where);
            
        }catch(SqlException $error)
        {
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604010);
        }
    }
    public function update(object $entity, array $_where)
    {   
        $table = Entity::getTable(get_class($entity));
        $result = self::$sql->query($entity->update($table, $_where));
    }
    public function fetchResponse(string $entity, string $responseClass, array|null $_where = null): array
    {     
        try{
            $results = self::$sql->fetch(Entity::getTable($entity), $_where);
            if($results == null ) return [];
            return array_map(function(array $results) use ($responseClass)
            {
                return new $responseClass($results);
            }, $results);
            
        }catch(SqlException $error)
        {
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604010);
        }
    }
    public static function param(string|null $parameters, $colum)
    {
        return $parameters ? [$colum, $parameters]: null;
    }
}