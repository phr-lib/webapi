<?php

namespace Phr\Webapi\ApiBase;


enum Errors 
{
    case E5100300;

    case E5600000;
    case E5605000;
    case E5605001;
    case E5605011;
    case E5605012;

    case E5605101;


    case E5604000;
    case E5604001;
    case E5604002;
    case E5604003;
    case E5604005;
    case E5604010;


    case E5604011;
    case E5604012;
    case E5604050;

    case E5655000;
    case E5655001;














    public function code(): int 
    {
        return match ($this) 
        {
            self::E5100300 => 5100300,
            self::E5600000 => 5600000,


            self::E5605000 => 5605000,
            self::E5605001 => 5605001,
            self::E5605011 => 5605011,
            self::E5605012 => 5605012,

            self::E5605101 => 5605101,

            self::E5604000 => 5604000,
            self::E5604001 => 5604001,
            self::E5604002 => 5604002,
            self::E5604003 => 5604003,
            self::E5604005 => 5604005,
            self::E5604010 => 56040010,


            self::E5604011 => 5604011,
            self::E5604012 => 5604012,
            self::E5604050 => 5604050,

            self::E5655000 => 5655000,
            self::E5655001 => 5655001,


            






        };
    }

    public function message(): string 
    {
        return match ($this) 
        {   
            self::E5100300 => 'Save file eror',
            self::E5600000 => 'Can not define applilcation variant',

            self::E5605000 => 'No Phr-Process header!',
            self::E5605001 => 'No Phr-Process header code',

            self::E5605011 => 'Phr-Process header is malformed',
            self::E5605012 => 'Phr-Process time header is unauthorized',

            self::E5605101 => 'Maintaince',


            self::E5604000 => 'Application registartion - no register action',
            self::E5604001 => 'Application registartion - already registrated',
            self::E5604002 => 'Registration update - application is not registered',
            self::E5604003 => 'Registration update - reset key failed',
            self::E5604005 => 'No main syss folder',
            self::E5604010 => 'No file',


            self::E5604011 => 'Global public rsa is already regitered',
            self::E5604012 => 'Private public rsa is already regitered',

            self::E5604050 => 'application key',

            self::E5655000 => 'jwt error',
            self::E5655001 => 'jwt expired'



        };
    }
}