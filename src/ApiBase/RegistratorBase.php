<?php

namespace Phr\Webapi\ApiBase;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiBase\Support\RegistrationParameters as RP;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;


use Phr\Webapi\ApiBase\Support\FileContent\ConfigFile;
use Phr\Webapi\ApiControl\ResponseCode as RC;

use Phr\Certificator\Encryption;
use Phr\Certificator\SaveFile;
use Phr\Certificator\FileHandler\ConfigContent;
use Phr\Certificator\FileHandler\Line\ConfigLine;
use Phr\Certificator\FileHandler\AplContent;
use Phr\Certificator\FileHandler\Line\AplLine;
use Phr\Certificator\Crips;
use Phr\Certificator\Encry\HashAlgo;
use Phr\Certificator\CertKey\BlockKey;
use Phr\Certificator\FileHandler\FileSettings;


/**
 * @abstract 56 04 000
 * 
 * 
 * @see Registration
 */
abstract class RegistratorBase
{   

    protected \DateTime $registrationDate;

    protected ConfigContent $configContent;

    protected AplContent $aplContent;

    public function __construct(string $_title)
    {
        $this->registrationDate = new \DateTime('now');
        $this->configContent = new ConfigContent(new ConfigLine(
            ConfigFile::DATE->value, 
            $this->registrationDate->format(SF::REGISTRATION_CONFIG_FILE->dateFormat())
        ));
        $this->aplContent = new AplContent($_title,new AplLine(
            ['Date', $this->registrationDate->format(SF::REGISTRATION_FILE->dateFormat())]
            ,'Registration certificate'
        ));
    }
    
    /**
     * @access protected
     */
    protected function createConfigFile(string|null $_filename = null)
    {
        $config = new SaveFile(SHELL::projectFolder().SF::REGISTRATION_CONFIG_FILE->folder(), new FileSettings(
            SF::REGISTRATION_CONFIG_FILE->fileType(),
            SHELL::settings()->solutionId,
            SHELL::settings()->applicationId
        ));
        $config->create($this->configContent, $_filename);
    }
    protected function createAplFile(string|null $_filename = null)
    {   
        $aplCert = new SaveFile(SHELL::syssFolder().SF::REGISTRATION_FILE->folder(), new FileSettings(
            SF::REGISTRATION_FILE->fileType(),
            SHELL::settings()->solutionId,
            SHELL::settings()->applicationId
        ));
        $aplCert->create($this->aplContent,  $_filename);
    }
    /**
     * @static
     */
    protected static function createDir(string $_dir)
    {
        if(!is_dir($_dir))
            mkdir($_dir, 0777);
    }
    protected static function generateNewApplicationKey(): void 
    {
        $newApplicationKey = new BlockKey(SHELL::projectFolder().SF::REGISTRATION_KEY->folder());
        $newApplicationKey->generate();
        $newApplicationKey->save(SHELL::applicationKey());
    }
    
    protected function fingerPrintsToResetKeys(array $_finger_prints, HashAlgo $ALGO): void
    {
        $this->configContent->add(new ConfigLine(ConfigFile::RK1->value, Encryption::hashIt($_finger_prints[0], $ALGO)));
        $this->configContent->add(new ConfigLine(ConfigFile::RK2->value, Encryption::hashIt($_finger_prints[1], $ALGO)));
        $this->configContent->add(new ConfigLine(ConfigFile::RK3->value, Encryption::hashIt($_finger_prints[2], $ALGO)));
        $this->configContent->add(new ConfigLine(ConfigFile::RK4->value, Encryption::hashIt($_finger_prints[3], $ALGO)));
        $this->configContent->add(new ConfigLine(ConfigFile::RK5->value, Encryption::hashIt($_finger_prints[4], $ALGO)));
    }
    /**
     * @method check reset key
     * @param object resetkeys
     * @param array config parameters form register.apl
     * @return true
     * @throws WebApiException
     */
    protected static function authenticateResetCodes(SF $SF, object $_resetKeys, HashAlgo $RESET_TYPE): true
    {   
        
        $initApl = SHELL::projectFolder().$SF->file();
        
        if(!file_exists($initApl)) throw new SF(RC::CONFLICT, ERR::E5604002);
        $aplConfig = SaveFile::read($initApl);
        
        if($aplConfig[ConfigFile::RK1->value] !== Encryption::hashIt($_resetKeys->resetKeys->rk1, $RESET_TYPE))
                throw new WebApiException(RC::UNAUTHORIZED, ERR::E5604003);
        
        return true;
    }
    
    
}