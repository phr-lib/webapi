<?php

namespace Phr\Webapi\ApiBase;

use Phr\Webapi\IApi;
use Phr\Shell\Shell as SHELL;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiBase\Errors as ERR;

use Phr\Webapi\Settings\ApiRoutes;
use Phr\Webapi\Settings\AppSettings;
use Phr\Webapi\Settings\DbSettings;
use Phr\Webapi\Settings\AppSecurity;
use Phr\Webapi\ApiControl\Authenticate;
use Phr\Webapi\ApiBase\Support\ApiVars as APP;

use Phr\Webapi\ApiControl\Authorization\HeaderAuthorization;
use Phr\Webapi\ApiControl\Authorization\Sessions\WebSession\ActiveUser;

/**
 * @abstract
 * 
 * @see Api
 */
abstract class WebApiBase
{   
    protected static string $method; 

    protected static ApiRoutes $apiRoutes;

    protected static AppSettings $settings;

    protected static string $root;

    protected static SHELL $SHELL;

    protected static string $sysFolder = 'syss/';

    protected static string $solutionFolder;

    protected static string $applicationKey;

    protected static APP $APP;

    protected static string $decryptedIncomeing;

    protected static ActiveUser $activeUser;

    public function __construct(string $_root)
    {  
        self::$root = $_root;
        self::$SHELL = new SHELL;        
    }
    /**
     * @method fast method not allowed 
     * response.
     */
    protected static function METHODNOTALLOWED()
    {
        return SHELL::response(RC::METHOD_NOT_ALLOWED);
    }
    /**
     * @method stores routes form index
     */
    public static function apiRoutes(array|null $rools = null)
    {   
        return self::$apiRoutes::$routes;
    }
    /**
     * @access protected
     * @method loads appconfig into running
     * shell thread!
     */
    protected function loadAppConfig()
    {  
        if(!file_exists(self::$root.IApi::APPCONFIG))
            throw new WebApiException(RC::NOT_IMLEMENTED);
        $configContent = json_decode(file_get_contents(self::$root.IApi::APPCONFIG));
        # RAM settings !
        self::$settings = new AppSettings(
            $configContent->project,
            $configContent->projectDescription,
            $configContent->solutionId,
            $configContent->application,
            $configContent->description,
            $configContent->applicaitonId,
            $configContent->appVar,
            new DbSettings(
                $configContent->database->host,
                $configContent->database->port,
                $configContent->database->shema
            ),
            new AppSecurity(
                $configContent->security->applicationKey,
                $configContent->security->maintainceKey
            )
        );
        #$this->saveAppVar($configContent->appVar);
        
    }
    
    /**
     * @method calculates solution and 
     * application folder form it's id.
     */
    protected function calculateApplicationFolders():void 
    {   
        self::$solutionFolder =  md5(self::extractIdKey(self::$settings->solutionId));
        self::$applicationKey =  md5(self::extractIdKey(self::$settings->applicationId));
    }
    /**
     * @method intercept url routes
     * and rerutes them on target controller.
     * If authentication is set it interceptd authentication
     * cridentials.
     */
    protected function pathController(): void
    {      
        HeaderAuthorization::controller();
        $apiRoutes = self::apiRoutes();
        $rootRoutes = [];
        foreach(array_keys($apiRoutes) as $key) array_push($rootRoutes, $key);
        
        

        if(in_array(Shell::route(), $rootRoutes))
        {  
            if(isset($apiRoutes[Shell::route()][1]))
            {   
                $secondPaths = [];
                foreach(array_keys($apiRoutes[Shell::route()][1]) as $key) array_push($secondPaths, $key);
                if(in_array(Shell::route(1), $secondPaths))
                {   
                    if(isset($apiRoutes[Shell::route()][1][Shell::route(1)][1]))
                    {
                        $thirdPath = [];
                        foreach(array_keys($apiRoutes[Shell::route()][1][Shell::route(1)][1]) as $key) array_push($thirdPath, $key);
                        if(in_array(Shell::route(2), $thirdPath))
                        {
                            $newControllerClass = $apiRoutes[Shell::route()][1][Shell::route(1)][1][Shell::route(2)][0];
                        }
                        else
                        {
                            $newControllerClass = $apiRoutes[Shell::route()][1][Shell::route(1)][1];
                        }
                        
                    }
                    else
                    {
                        $newControllerClass = $apiRoutes[Shell::route()][1][Shell::route(1)][0];
                    }
                    
                }
                else
                {    
                    $newControllerClass = new $apiRoutes[Shell::route()][0];
                }
                
            }
            else
            {    
                $newControllerClass = new $apiRoutes[Shell::route()][0];
            }
                 
            $newControllerClass->controller();
        }
        
    }
    /**
     * @access private
     * @method defines application.
     * @param string appliaction variant from settings.
     */
    private function saveAppVar(string $_app_var): void 
    {
        switch ( $_app_var ) 
        {
            case APP::APPLICATION_BASIC->value: self::$APP = APP::APPLICATION_BASIC; break;
            
            default: throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5600000); break;
        }
    }
    private static function extractIdKey(string $_id_key): string
    {
        $r = explode('::', $_id_key);
        return $r[1];
    }
    
}