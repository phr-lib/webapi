<?php

namespace Phr\Webapi\ApiBase;

use Phr\Webapi\Api;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiBase\Support\RegistrationParameters as RP;
use Phr\Webapi\ApiControl\ResponseCode as RC;

use Phr\Certificator\Encryption;
use Phr\Certificator\Encry\HashAlgo;


/**
 * 
 * @abstract maintaince
 * 
 * 
 * 
 */
abstract class MaintainceBase
{   
    /**
     * 
     * Registration parameters
     * 
     * 
     * 
     */
    protected static RP $RP;

    /// CONSTRUCTOR ***
    public function __construct(string|null $_registrator_encry_parameters)
    {   
        /**
         * Catch parematers
         * action
         */
        if($_registrator_encry_parameters != null)
        {
            $parameters = self::parameters($_registrator_encry_parameters);

            if($parameters){

                switch ( $parameters['action'] ) 
                {   
                    /**
                     * Create new key.
                     */
                    case RP::NEW_KEY->code(): self::$RP = RP::NEW_KEY; break;
                    /**
                     * Create new global rsa key pairs
                     */
                    case RP::NEW_RSA->code(): self::$RP = RP::NEW_RSA; break;
                    case RP::NEW_CERT->code(): self::$RP = RP::NEW_CERT; break;
                    case RP::NEW_IDP_CERT->code(): self::$RP = RP::NEW_IDP_CERT; break;
                    case RP::MIGRATIONS->code(): self::$RP = RP::MIGRATIONS; break;

        
                    default: throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604000); break;
                }
            }
        }
    }
    
    /**
     * @static
     * @method fetches application key
     * from application security.
     * @return string application key. 
     */
    protected static function applicationKey(): string
    {
        return Api::settings()->appSecurity->applicationKey;
    }
    /**
     * @access private
     * @method decodes incomeng parameters
     * @param string registartion parameters
     * @return object incomeing
     */
    private static function parameters(string $_registrator_parameters): array|null
    {   
        $parameters = [];
        $params = urldecode($_registrator_parameters);
        if(preg_match('/=/', $params))
        {
            foreach (explode('&', $params) as $pr)
            {
                $param = explode("=", $pr);
                $parameters[$param[0]] = $param[1];
            }
            return $parameters;
        }return null;
        

    }
}