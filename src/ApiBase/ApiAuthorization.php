<?php

namespace Phr\Webapi\ApiBase;

use Phr\Shell\Shell;
use Phr\Shell\IShell;
use Phr\Shell\Http\ResponseCode;
use Phr\Shell\ShellBase\Authorization;

use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiBase\Support\ProcessHeaderTypes as HT;
use Phr\Webapi\ApiBase\Support\ActiveAuthorization as ACTIVEAUTH;
use Phr\Webapi\Utility\Maintaince;
use Phr\Webapi\Settings\AppSecurity;

/**
 * 
 * 
 * @abstract header authorisation
 * controller.
 * 
 * Base header controll unit.
 * 
 * 
 */
abstract class ApiAuthorization
{   
    /**
     * 
     * @var string for header
     * identification.
     * 
     */
    private const HEADER_DELIMITER = '#PHR';
    /**
     * Active authorization, 
     * for further use.
     */
    protected static ACTIVEAUTH $ACTIVE;
    /**
     * @static
     * @method header intercepter. Intercept
     * Phr-process header. 
     * @param string|null application key.
     */
    public static function header(AppSecurity|null $_app_security = null)
    {   
        /**
         * Get process header
         * Phr-process
         */
        $header = Shell::process();
        /**
         * With no header, call is refused.
         * expectation failed header is returned
         * to client
         */
        if(!isset($header)) throw new WebApiException(RC::EXPECTATION_FAILED, ERR::E5605000);
        /**
         * Header controller.
         */
        switch ( substr($header,1,3) ) 
        {   
            /**
             * Header code ###
             */
            case HT::HEADER->code(): self::$ACTIVE = ACTIVEAUTH::NOAUTH; break;
            /**
             * Header code $%$
             */
            case HT::TIME_HEADER->code(): self::checkTimeHeader($header); break;
            /**
             * Header code %$%
             */
            case HT::APPLICATION_HEADER->code(): self::checkApplicationHeader($header, $_app_security); break;
            /**
             * Header code %%%
             */
            case HT::MAINTAINCE_HEADER->code(): self::checkMaintaiceHeader($header); break;
            /**
             * With no correct process code
             */
            default: throw new WebApiException(RC::EXPECTATION_FAILED, ERR::E5605001); break;
        }
    }
    private static function checkTimeHeader( string $_header )
    {   
        if (preg_match('/#PHR/', $_header, $matches)){
            $headerParts = explode(self::HEADER_DELIMITER, $_header);
            if(md5(substr(time(),0,8)) !== $headerParts[1])
                throw new WebApiException(RC::NOT_ACCEPTABLE, ERR::E5605012);
        }else throw new WebApiException(RC::EXPECTATION_FAILED, ERR::E5605011);
    }
    private static function checkApplicationHeader( string $_header, string|null $_application_key )
    {   
        if(!isset($_application_key))
            throw new WebApiException(RC::CONFLICT, 'no application key set for this authentication');

        $encry = md5(substr(time(),0,8).$_application_key);
        $headerParts = explode(self::HEADER_DELIMITER, $_header);
        if($encry !== $headerParts[1])
            throw new WebApiException(RC::FORBIDDEN, 'sealed');
    }
    private static function checkMaintaiceHeader(string $_header)
    {
        self::$ACTIVE = ACTIVEAUTH::MAINTAINCE;

        $utility = new Maintaince;
        $utility->controller($_header);

    }
}