<?php

namespace Phr\Webapi\ApiBase\Support;

use Phr\Certificator\Encry\HashAlgo;


enum RegistrationParameters 
{
    private const NEWKEYCODE = 'PHRAPKEYNEWSEQUENCE';

    private const NEWRSACODE = 'PHRSSHRSASEQUENCE';

    private const MAINTAINCECODE = 'PHRSSHRSASEQUENCE';

    private const MIGRATIONCODE = 'PHRMIGRATIONSEQUENCE';



    case NEW_KEY;

    case NEW_RSA;

    case MAINTAINCE;

    case MIGRATIONS;


    public function code(): string 
    {
        return match ($this) {
            self::NEW_KEY => self::NEWKEYCODE,
            self::NEW_RSA => self::NEWRSACODE,
            self::MAINTAINCE => self::MAINTAINCECODE,
            self::MIGRATIONS => self::MIGRATIONCODE,


        };
    }
    public function algo(): HashAlgo 
    {
        return match ($this) {
            self::NEW_KEY => HashAlgo::SHA256,
            self::NEW_RSA => HashAlgo::SHA512,
            self::MAINTAINCE => HashAlgo::SHA512,
            self::MIGRATIONS => HashAlgo::SHA512,

        };
    }

}