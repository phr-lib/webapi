<?php

namespace Phr\Webapi\ApiBase\Support\FileContent;

enum ConfigFile: string 
{
    case DATE = 'DATE';

    case REG_ID = 'REGID';

    case DB_REG_ID = 'DBREGID';

    case RSA_REG_ID = 'RSAREGID';

    case RK1 = 'rk1';

    case RK2 = 'rk2';

    case RK3 = 'rk3';

    case RK4 = 'rk4';

    case RK5 = 'rk5';

}