<?php

namespace Phr\Webapi\ApiBase\Support\FileContent;

enum ConfigMigrations: string 
{
    case DATE = 'DATE';

    case MIGRATION = 'MIGID';

}