<?php

namespace Phr\Webapi\ApiBase\Support;


enum ActiveAuthorization 
{
    case NOAUTH;

    case AUTHTIME;

    case APPLICATION;

    case MAINTAINCE;

}