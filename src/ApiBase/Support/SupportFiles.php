<?php

namespace Phr\Webapi\ApiBase\Support;

use Phr\Certificator\FileHandler\FileVars;
use Phr\Certificator\ISaveFile;
use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;

enum SupportFiles
{
    case REGISTRATION_KEY;

    case LOG;

    case STORAGE;

    case SESSION;

    case REGISTRATION_FILE;

    case REGISTRATION_CONFIG_FILE;

    case GLOBAL_PUBLIC_RSA;

    case GLOBAL_PRIVATE_RSA;

    case GLOBAL_PEM_CONFIG;

    case DB_CERT;

    case DB_CERT_APL;

    case DB_CERT_CONF;

    case RSA_CERT_APL;

    case RSA_CERT_CONFIG;

    case SETUP_APL;

    case SESSION_CERT;

    case MIGRATION_CONF;

    public function folder(): string 
    {
        return match ($this) {
            self::REGISTRATION_KEY => '/',
            self::LOG => 'logs/',
            self::STORAGE => 'stor/',
            self::SESSION => '/sess/',
            self::REGISTRATION_FILE => 'regs/',
            self::REGISTRATION_CONFIG_FILE => '/regs/',
            self::GLOBAL_PUBLIC_RSA => '/crts/',
            self::GLOBAL_PRIVATE_RSA => '/crts/',
            self::GLOBAL_PEM_CONFIG => '/regs/',
            self::DB_CERT => '/crts/',
            self::DB_CERT_APL => 'regs/',
            self::DB_CERT_CONF => '/regs/',
            self::RSA_CERT_APL => 'regs/',
            self::RSA_CERT_CONFIG => '/regs/',
            self::SETUP_APL => 'regs/',
            self::SESSION_CERT => '/sess/',
            self::MIGRATION_CONF => '/regs/',
        };
    }
    public function fileName(): string 
    {
        return match ($this) {
            self::REGISTRATION_KEY => '',
            self::REGISTRATION_FILE => 'registration',
            self::REGISTRATION_CONFIG_FILE => 'registration',
            self::GLOBAL_PUBLIC_RSA => 'global.public.pem',
            self::GLOBAL_PRIVATE_RSA => 'global.private.pem',
            self::GLOBAL_PEM_CONFIG => 'global',
            self::DB_CERT_APL => 'dbregistration',
            self::DB_CERT_CONF => 'dbregistration',
            self::RSA_CERT_APL => 'secure',
            self::RSA_CERT_CONFIG => 'secure',
            self::SETUP_APL => 'bsset',
            self::MIGRATION_CONF => 'migrations'
        };
    }
    public function file(): string 
    {
        return match ($this) {
            self::REGISTRATION_KEY => self::REGISTRATION_FILE->folder(),
            self::REGISTRATION_FILE =>          self::REGISTRATION_FILE->folder()
                                                .self::REGISTRATION_FILE->fileName()
                                                .GR::COMA.FileVars::FILE_APL->ext(),
                                                
            self::REGISTRATION_CONFIG_FILE =>   self::REGISTRATION_CONFIG_FILE->folder()
                                                .self::REGISTRATION_CONFIG_FILE->fileName()
                                                .GR::COMA.FileVars::FILE_SFCRY->ext(),
            
            self::GLOBAL_PUBLIC_RSA =>          self::GLOBAL_PUBLIC_RSA->folder()
                                                .self::GLOBAL_PUBLIC_RSA->fileName(),
            
            self::GLOBAL_PRIVATE_RSA =>          self::GLOBAL_PRIVATE_RSA->folder()
                                                .self::GLOBAL_PRIVATE_RSA->fileName(),
        
            self::GLOBAL_PEM_CONFIG =>          self::GLOBAL_PEM_CONFIG->folder()
                                                .self::GLOBAL_PEM_CONFIG->fileName()
                                                .GR::COMA.FileVars::FILE_SFCRY->ext(),
            
            self::DB_CERT_APL =>                self::DB_CERT_APL->folder()
                                                .self::DB_CERT_APL->fileName()
                                                .GR::COMA.FileVars::FILE_APL->ext(),
            
            self::DB_CERT_CONF =>                self::DB_CERT_CONF->folder()
                                                .self::DB_CERT_CONF->fileName()
                                                .GR::COMA.FileVars::FILE_SFCRY->ext(),

            self::RSA_CERT_APL =>                self::RSA_CERT_APL->folder()
                                                .self::RSA_CERT_APL->fileName()
                                                .GR::COMA.FileVars::FILE_APL->ext(),
            
            self::RSA_CERT_CONFIG =>                self::RSA_CERT_CONFIG->folder()
                                                .self::RSA_CERT_CONFIG->fileName()
                                                .GR::COMA.FileVars::FILE_SFCRY->ext(),
            
            self::SETUP_APL =>                self::SETUP_APL->folder()
                                                .self::SETUP_APL->fileName()
                                                .GR::COMA.FileVars::FILE_APL->ext(),

            self::MIGRATION_CONF =>            self::MIGRATION_CONF->folder()
                                                .self::MIGRATION_CONF->fileName()
                                                .GR::COMA.FileVars::FILE_SFCRY->ext(),
        };
    }
    public function fileType(): string 
    {
        return match ($this) {
            self::REGISTRATION_KEY => 'KEY',
            self::REGISTRATION_FILE => ISaveFile::FILE_APL,
            self::REGISTRATION_CONFIG_FILE => ISaveFile::FILE_SFCRY,
            self::GLOBAL_PEM_CONFIG => ISaveFile::FILE_SFCRY,
            self::DB_CERT => ISaveFile::CERT_SFCRT_SIGNED,
            self::DB_CERT_APL => ISaveFile::FILE_APL,
            self::DB_CERT_CONF => ISaveFile::FILE_SFCRY,
            self::RSA_CERT_APL => ISaveFile::FILE_APL,
            self::RSA_CERT_CONFIG => ISaveFile::FILE_SFCRY,
            self::SETUP_APL => ISaveFile::FILE_APL,
            self::SESSION_CERT => ISaveFile::CERT_SFCRT_SIGNED,
            self::MIGRATION_CONF => ISaveFile::FILE_SFCRY,
        };
    }
    public function dateFormat(): string 
    {
        return match ($this) {
            self::REGISTRATION_KEY => 'Y-m-d\\TH:i:sP',
            self::REGISTRATION_FILE => 'Y-m-d\\TH:i:sP',
            self::REGISTRATION_CONFIG_FILE => 'Y-m-d\\TH:i:sP',
            self::GLOBAL_PEM_CONFIG => 'Y-m-d\\TH:i:sP',
            self::DB_CERT_APL => 'Y-m-d\\TH:i:sP',
            self::DB_CERT_CONF => 'Y-m-d\\TH:i:sP',
            self::RSA_CERT_APL => 'Y-m-d\\TH:i:sP',
            self::RSA_CERT_CONFIG => 'Y-m-d\\TH:i:sP',
            self::SETUP_APL => 'Y-m-d\\TH:i:sP',
            self::MIGRATION_CONF => 'Y-m-d\\TH:i:sP',
        };
    }
    public function ext(): string 
    {
        return match ($this) {
            self::REGISTRATION_KEY => 'KEY',
            self::DB_CERT_APL => FileVars::FILE_APL->ext(),
            self::DB_CERT => 'cert',
            self::SETUP_APL => 'apl',
            self::SESSION_CERT => 'cert',
            self::MIGRATION_CONF => 'cert',
        };
    }
}