<?php

namespace Phr\Webapi\ApiBase\Support;


enum ProcessHeaderTypes 
{   
    private const HEADERDELIMITER = '#PHR';
    
    private const HEADERCODE = '###';

    private const TIMEHEADERCODE = '%$%';

    private const APPHEADERCODE = '$%$';

    private const MAINTAINCEHEADERCODE = '%%%';

    case HEADER_DELIMITER;
    
    case HEADER;

    case TIME_HEADER;

    case APPLICATION_HEADER;

    case MAINTAINCE_HEADER;


    public function code(): string 
    {
        return match ($this) {
            self::HEADER_DELIMITER => self::HEADERDELIMITER,
            self::HEADER => self::HEADERCODE,
            self::TIME_HEADER => self::TIMEHEADERCODE,
            self::APPLICATION_HEADER => self::APPHEADERCODE,
            self::MAINTAINCE_HEADER => self::MAINTAINCEHEADERCODE,
        };
    }
}