<?php

namespace Phr\Webapi\ApiBase;

use Phr\Shell\Shell as SHELL;
use Phr\Shell\Http\IHttpMethod;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Eojwt\EoJwtException;


use Phr\Webapi\Settings\AppSettings;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;
use Phr\Certificator\SaveFile;
use Phr\Certificator\SaveFileError;
use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Eojwt\EoJwt;
use Phr\Webapi\ApiControl\Authorization\Sessions\SessionControl;

class ApiShell extends WebApiBase implements IHttpMethod
{   
    public static function method(): string
    {
        return Shell::method();
    }
    public static function route(int $_route = 0): string|null
    {
        return Shell::route($_route);
    }
    public static function incomeing()
    {
        return Shell::incomeing();
    }
    public static function posted()
    {
        return Shell::posted();
    }
    public static function syssFolder(): string
    {
        return parent::$root . self::$sysFolder;
    }
    public static function projectFolder(): string
    {
        return parent::$root . self::$sysFolder .'.'.self::$solutionFolder;
    }
    public static function masterKey(): string
    {
        return self::projectFolder().'/'.self::$applicationKey.'.key';
    }
    public static function applicationKey(): string
    {
        return self::$applicationKey;
    }

    public static function settings(): AppSettings
    {   
        return parent::$settings;
    }
    public static function parameters(): string|int|null
    {  
        return SHELL::parameters();
    }
    public static function selectByParameter(string|int $_db_param): string|int|null
    {  
        if(SHELL::parameters() == null) return null;
        else {
            return [$_db_param, SHELL::parameters()];
        }      
    }
    public static function process(): string|null
    {
        return SHELL::process();
    }
    public static function authorization(): string
    {
        return SHELL::authorize();
    }
    public static function setDecryptedIncomeing(string $_decrypted): void 
    {
        self::$decryptedIncomeing = $_decrypted;
    }
    public static function decryptedIncomeing(): string 
    {
        return self::$decryptedIncomeing;
    }

    public static function response(RC $_response_code, object|string $_content = null)
    {   
        if($_response_code == RC::ACCEPTED)
        {
            Shell::setResponseHeader($_content);
            $content = null;
            
        }else $content = $_content;
            
        Shell::response($_response_code->value, $content);
    }
    public static function secureResponse(object $_content, RC $_response_code = RC::OK)
    {      
        $entyptedResponse = ApiSecure::enryptResponse($_content);  
        Shell::secureResponse($entyptedResponse, $_response_code->value);
    }
    public static function aplExists(SF $SF): bool
    {   
        $file = self::syssFolder().$SF->file();
        if(!file_exists($file))
                    return false;
        else return true;
    }
    public static function retriveFile(SF $SF)
    {   
        $file = self::projectFolder().$SF->file();
        try
        {   
            if(!file_exists($file))
                    throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604010);

            return SaveFile::read($file);
                        
        }catch(SaveFileError $error)
        {   
            $code = $error->getCode();
            $message = $error->getMessage();
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604050,"[E:loadfile:{$code}] ::{$message}[e]");
        }
    }
    public static function retriveDbcert(): array
    {   
        $crts = ApiSecure::dbCertHash();
        $file = self::projectFolder().SF::DB_CERT->folder().$crts[1].'.'.SF::DB_CERT->ext();
        
        try
        {   
            if(!file_exists($file))
                    throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604010);
            $fullCertContent = SaveFile::read($file, $crts[0]);
            return $fullCertContent->content->dbKeys;
                        
        }catch(SaveFileError $error)
        {   
            $code = $error->getCode();
            $message = $error->getMessage();
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604050,"[E:loadfile:{$code}] ::{$message}[e]");
        }
    }
    public static function migrationFile(): array
    {
        $directory = 'Migrations';    
        return array_diff(scandir($directory), array('..', '.', 'Migrations.php'));
    }
    public static function migrationClass(string $dir): string 
    {
        $dirp = explode('.', $dir);
        return $dirp[0];
    }
    public static function migrationId(string $dir): string 
    {
        $dirp = explode('_', $dir);
        return $dirp[1];
    }
    public static function sessionControl()
    {
        $token = self::authorization();
        try{
            $jwt = new EoJwt($token);
            $payload = $jwt->payload();     
            self::$activeUser = SessionControl::activeSessionControl($jwt->payload());
            
        }catch(EoJwtException $error)
        {   
            if(5393501 == $error->getCode())
            throw new WebApiException(RC::UNAUTHORIZED, ERR::E5655001, $error->getMessage());
            if(5393502 == $error->getCode())
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604010, $error->getCode());
        }
        
    }
}