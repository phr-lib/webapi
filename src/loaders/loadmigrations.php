<?php

interface Register 
{   
    /**
     * Set library source
     * Loaded without namespace prefix
     * @var string lib 
     */
    public const LIB_SOURCE = 'Migrations';
    /**
     * Set app namespace source
     * Loaded with namespace
     * @var string *
     */
    public const APP_SOURCE = 'Construct';

    /**
     * Languange extention
     * @var string
     */
    public const EXT = '.php';
    /**
     * Phr Library extention class
     * @var string
     */
    public const LIB_EXT = '.migration.php';
}
/**
 * GLOBAL Phr AUTOLOADER
 */
spl_autoload_register(function($classname){
    
    if(ctype_upper($classname)){}
    else
    {
    $FullClassPath = $ClassInArray = explode("\\", $classname);
    
    #set target route
    $SourceRoute = array_shift($ClassInArray);
    # set target file
    $ClassFile = array_pop($ClassInArray);
    
    $IncludeNamespaceLibrary =  ROOT.
                                Register::LIB_SOURCE.
                                DIRECTORY_SEPARATOR;

    foreach($ClassInArray as $path)
        $IncludeNamespaceLibrary .=  $path . DIRECTORY_SEPARATOR;
    $IncludeNamespaceLibrary .= $ClassFile . Register::LIB_EXT;
    
    if(file_exists($IncludeNamespaceLibrary))
        include_once $IncludeNamespaceLibrary; 
    }
});