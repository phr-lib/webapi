<?php

interface Register 
{   
    /**
     * Set library source
     * Loaded without namespace prefix
     * @var string lib 
     */
    public const LIB_SOURCE = 'Controllers';
    /**
     * Set app namespace source
     * Loaded with namespace
     * @var string *
     */
    public const APP_SOURCE = 'Construct';

    /**
     * Languange extention
     * @var string
     */
    public const EXT = '.php';
    /**
     * Phr Library extention class
     * @var string
     */
    public const LIB_EXT = '.controller.php';
    /**
     * Php Library subclasses
     * @var string 
     */
    public const LIB_CLASS = '/routes/';

}
/**
 * GLOBAL Phr AUTOLOADER
 */
spl_autoload_register(function($classname){

    $FullClassPath = $ClassInArray = explode("\\", $classname);

    #set target route
    $SourceRoute = array_shift($ClassInArray);

    # set target file
    $ClassFile = array_pop($FullClassPath);
    
    $IncludeNamespaceLibrary =   ROOT . ($SourceRoute == Register::LIB_SOURCE ? 
    (count($FullClassPath) <= 2 ? 
    implode(DIRECTORY_SEPARATOR, $FullClassPath)
    .DIRECTORY_SEPARATOR
    .$ClassFile
    .Register::EXT
    :
    implode(DIRECTORY_SEPARATOR, array_slice($FullClassPath,0, 2))
    .Register::LIB_CLASS
    .implode(DIRECTORY_SEPARATOR, array_slice($FullClassPath,2, 9))
    .DIRECTORY_SEPARATOR
    .$ClassFile
    .Register::LIB_EXT
    )
    :
    Register::APP_SOURCE.DIRECTORY_SEPARATOR
    .implode(DIRECTORY_SEPARATOR, $FullClassPath)
    .DIRECTORY_SEPARATOR
    .$ClassFile.Register::EXT
    );
    if(file_exists($IncludeNamespaceLibrary)) include_once $IncludeNamespaceLibrary;    
    


});