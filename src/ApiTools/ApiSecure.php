<?php

namespace Phr\Webapi\ApiTools;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiBase\Support\SupportFiles;
use Phr\Webapi\ApiControl\ResponseCode as RC;

use Phr\Certificator\CertKey\BlockKey;
use Phr\Certificator\Encryption;
use Phr\Certificator\Encry\HashAlgo;

use Phr\Webapi\ApiControl\Contracts\Secure\PublicKeyResponse;


/**
 * 
 * @final
 * 
 * 
 */
final class ApiSecure
{   

    private static string $encryptor;

    public static function enryptResponse(object $_content, string $iv = "2354658123546521"): string
    {   
        $enrcy = Encryption::sslEncryptOptions(json_encode($_content), self::$encryptor, $iv);
        return Encryption::encodeHex($enrcy);
    }
    
    public static function digestEncry(): void
    {   
        $decryptedKey = Encryption::sslPrivateDecrypt(self::retrivePrivateRsa(), SHELL::authorization());
        if(!$decryptedKey) throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604050);
        
        if(SHELL::method() == SHELL::POST)
        {   
            $decrypt = new Encryption($decryptedKey);
            $payloadHex = SHELL::incomeing();
            $payload = Encryption::decodeHex($payloadHex);
            SHELL::setDecryptedIncomeing($decrypt->fernetDecrypt($payload));
        }
        elseif(SHELL::method() == SHELL::GET)
        {
            $result = SHELL::authorization();
            self::$encryptor = $result;            
        }        
    }
    public static function dbCertHash(): array
    {
        $key = self::retriveApplicationKey();
        $crypt = $key[0]*$key[1]*$key[2];
        $dbHash = Encryption::hashIt($key[0]*$key[1]*$key[2], HashAlgo::SHA256);
        $certFileName = Encryption::hashIt($key[0]*$key[1]*$key[2], HashAlgo::MD2);
        return [$dbHash, $certFileName];
    }
    public static function publicRsaHex(): string
    {
        $file = SHELL::projectFolder().SupportFiles::GLOBAL_PUBLIC_RSA->file();
        return Encryption::encodeHex(file_get_contents($file));
    }
    public static function retrivePublicRsa(): string
    {
        $file = SHELL::projectFolder().SupportFiles::GLOBAL_PUBLIC_RSA->file();
        return file_get_contents($file);
    }
    public static function retrivePrivateRsa(): string
    {
        $file = SHELL::projectFolder().SupportFiles::GLOBAL_PRIVATE_RSA->file();
        return file_get_contents($file);
    }
    
    public static function retriveApplicationKey()
    {
        try
        {   
            return BlockKey::load(SHELL::projectFolder().SupportFiles::REGISTRATION_KEY->folder().SHELL::applicationKey());
                        
        }catch(\Exception $error)
        {   
            $code = $error->getCode();
            $message = $error->getMessage();
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604050,"[E:loadkey:{$code}] application registration::{$message}[e]");
        }
    }
    /**
     * @access public
     * @static
     * @method calculates database cridentials
     * username and password
     * @return array [username, password]
     * @throws WebApiException
     */
    public static function dbKeyCridetials(array|null $_block_key1 = null,array|null $_block_key3 = null): array
    {   
        return self::calculateDbCridentials(self::retriveApplicationKey(), $_block_key1, $_block_key3);       
    }
    public static function createFingerPrint(int $_length = 5): array
    {   
        $fingerPrints = [];

        for($i = 0; $i < $_length; $i++)
        {
            $blocks = self::randomFp();
            array_push($fingerPrints, implode('-', $blocks));
        }
        return $fingerPrints;

    }
    private static function randomFp(int $_length = 5): array
    {   
        $fps = [];
        for($i = 0; $i < $_length; $i++)
        {
            array_push($fps, chr(rand(65,90)).rand(0,9).chr(rand(97,122)));
        }
        return $fps;
    }
    /**
     * @access private
     * @static
     */
    private static function calculateDbCridentials(
        array $_key_blocks,
        array|null $_key_block_certificate = null,
        array|null $_key_block_idp = null,
        int $_key_length = 18
    ): array {   
        /**
         * Clone key block, based on 
         * provided nubers of keys.
         */
        if($_key_block_certificate == null) $_key_block_certificate = $_key_blocks;
        if($_key_block_idp == null) $_key_block_certificate = $_key_blocks;
        /**
         * Loop thrue algo, regardless of 
         * key provided. 
         */
        $encryCode1 =  array_merge(str_split((string)$_key_blocks[0]),str_split((string)$_key_blocks[1]));
        $encryCode2 =  array_merge(str_split((string)$_key_block_certificate[2]),str_split((string)$_key_block_certificate[3]));
        $encry1 =  (int)implode((string)$_key_blocks[4], $encryCode1);
        $encry2 =  (int)implode((string)$_key_blocks[4], $encryCode2);
        $encry = $encry1.$encry2;
        $encry = substr($encry, 0, $_key_length);
        /**
         * Calculate password based
         * on numerc key
         */
        $password = self::calculateDbPassword((string)$encry);
        /**
         * Calculate username based
         * on numerc key
         */
        $username = self::calculateUsername((string)$encry);
        
        return [$username, $password];
    }


    private const MATIX = [
        ['?','@','}s','b{','%','$','/','b','Q','#'],
        ['a','@r','f','{f','v%','$d','n/','vb','%Q','q#'],
        ['b','t@','&','e{','s%','$ ','/n','bu','oQ','d#'],
        ['c','z@','u','v{','%d','$s','/s','vb','wQ','#l'],
        ['d','h','}o','d{','%h','$r','/f','bb','Qf','#v'],
        ['?','c','}u','{v','%r','$g','m/','wb','Qu','#k'],
        ['e','@b','s}','{n','d%','d$','/d','mb','vQ','s#'],
        ['f','r@','}v','q{','b%','k$','/l','bl','sQ','v#'],
        ['g','@b','q','n','%n','$v','v/','bv','Qm','#m'],
        ['i','e@','w','m','%z','$s','d/','bu','lQ','#o'],
        ['o','/','g','l','%,','o$','/l','bp','Qw','u#']
        ];
    private static function calculateDbPassword(string $_numerc_key): string
    {   
        $passPhrase = "";
        $encry =  str_split($_numerc_key);
        foreach($encry as $cr)
        {
            $passPhrase .= self::MATIX[(int)$cr+1][(int)$cr];
        }
        return $passPhrase;
    }
    private static function calculateUsername(string $_numerc_key)
    {
        return md5($_numerc_key);
    }
}