<?php

namespace Phr\Webapi\ApiTools\Keys;


readonly class DbKeys 
{   
    public array $dbKeys;

    public function __construct(string $_db_codes)
    {
        $this->dbKeys = explode('::', $_db_codes);
    }
}