<?php

namespace Phr\Webapi\ApiTools\Registration;

use Phr\Webapi\Api;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\RegistratorBase;
use Phr\Certificator\CertKey\BlockKey;
use Phr\Certificator\FileHandler\FileSettings;
use Phr\Certificator\SaveFile;
use Phr\Certificator\FileHandler\ConfigContent;
use Phr\Webapi\ApiBase\ApiSecure;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiTools\Keys\ResetKeys;
use Phr\Webapi\ApiControl\Contracts\KeyRegisterResponse;
use Phr\Certificator\Encry\RsaKeyGenerator;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiControl\Contracts\RsaRegistrationResponse;
use Phr\Certificator\SaveFileError;
use Phr\Certificator\FileHandler\AplContent;
use Phr\Certificator\FileHandler\Line\AplLine;
use Phr\Certificator\FileHandler\Line\ConfigLine;
use Phr\Certificator\Crips;
use Phr\Webapi\ApiBase\Support\SupportFiles;
use Phr\Webapi\ApiBase\Support\FileContent\ConfigFile;
use Phr\Certificator\Encry\HashAlgo;


/**
 * Register application key
 * 
 * 
 * 
 * 
 */
class KeyRegister extends RegistratorBase
{   
    public static function createNewRsa(object $_posted_reset_keys): RsaRegistrationResponse
    {   
        self::authenticateResetCodes($_posted_reset_keys, self::$BASE_RESET);
        
        $path = Api::projectFolder();
        if(file_exists(Api::projectFolder().SupportFiles::GLOBAL_PUBLIC_RSA->file()))
            throw new WebApiException(RC::CONFLICT, ERR::E5604011);
        if(file_exists(Api::projectFolder().SupportFiles::GLOBAL_PRIVATE_RSA->file()))
            throw new WebApiException(RC::CONFLICT, ERR::E5604011);
        $pemCreationDate = new \DateTime('now'); 
        # Generate rsa keys
        $rsaGenerator = new RsaKeyGenerator();
        $rsaGenerator->createKeyPairs();
        $rsaGenerator->saveKeys(
            Api::projectFolder().SupportFiles::GLOBAL_PUBLIC_RSA->folder(),
            'global'
        );
        # Generate new application key
        self::generate();
        /**
         * Start config content
         */
        $configContent = new ConfigContent(new ConfigLine(ConfigFile::DATE->value, SupportFiles::GLOBAL_PEM_CONFIG->dateFormat()));
        /**
         * Create new databse cridentials
         * based on new key.
         */
        $dbCridetials = ApiSecure::dbKeyCridetials();
        
        try
        {
            $pemConfig = new SaveFile(Api::projectFolder().SupportFiles::GLOBAL_PEM_CONFIG->folder(), new FileSettings(
                SupportFiles::GLOBAL_PEM_CONFIG->fileType(),
                Api::settings()->solutionId,
                Api::settings()->applicationId
            ));

            
            $resetKeys = ApiSecure::createFingerPrint(15);
            $configContent = self::fingerPrintsToResetKeys($configContent, $resetKeys, self::$PEM_RESET);

            $pemConfig->create($configContent, SupportFiles::GLOBAL_PEM_CONFIG->fileName());

            return new RsaRegistrationResponse($rsaGenerator->publicKey(), new KeyRegisterResponse(
                $dbCridetials[0],
                $dbCridetials[1],
                new ResetKeys($resetKeys)
            ));

        }catch(SaveFileError $error)
        {
            $code =  $error->getCode();
            $message =  $error->getMessage();
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, "[E:savefile:{$code}] application registration::{$message}[e]");
        }
        
        
    }
    
    public static function createNewKey(): KeyRegisterResponse
    {     
        # Check if register.apl exists!
        if(file_exists(Api::projectFolder().SupportFiles::REGISTRATION_FILE->file()))
            throw new WebApiException(RC::CONFLICT, ERR::E5604001 );
        self::createDir(Api::projectFolder());

        $dir =  Api::projectFolder().SupportFiles::REGISTRATION_FILE->folder();
        self::createDir($dir);

        $crt =  Api::projectFolder().SupportFiles::GLOBAL_PUBLIC_RSA->folder();
        self::createDir($crt);
        
        # Create and save application key.
        self::generate();

        # Create apl certificate
        $resetKeys = self::createAplCertAndConfigFile();
        
        # Temp generate db cridetials for response.
        $dbCridetials = ApiSecure::dbKeyCridetials();

        return new KeyRegisterResponse($dbCridetials[0], $dbCridetials[1], $resetKeys);
        
    }
    protected static function generate(): void 
    {
        $newApplicationKey = new BlockKey(Api::projectFolder().SupportFiles::REGISTRATION_KEY->folder());
        $newApplicationKey->generate();
        $newApplicationKey->save(Api::applicationKey());
    }
    /**
     * 
     * @static
     * @method creates .conf and .apl file
     * @return ResetKeys
     * @throws WebApiException
     * 
     */
    public static function createAplCertAndConfigFile(): ResetKeys
    {   
        try
        {   
            /**
             * Init config file
             */
            $config = new SaveFile(Api::projectFolder().SupportFiles::REGISTRATION_CONFIG_FILE->folder(), new FileSettings(
                SupportFiles::REGISTRATION_CONFIG_FILE->fileType(),
                Api::settings()->solutionId,
                Api::settings()->applicationId
            ));
            /**
             * Set application registration date
             */
            $registrationDate = new \DateTime('now');
            $aplContent = new AplContent('APPLICATION REGISTRATION',new AplLine(
                ['Date', $registrationDate->format(SupportFiles::REGISTRATION_CONFIG_FILE->dateFormat())], "Aplication registration date"
            ));
            $configContent = new ConfigContent(new ConfigLine(
                ConfigFile::DATE->value,
                $registrationDate->format(SupportFiles::REGISTRATION_CONFIG_FILE->dateFormat())
            ));
            /**
             * Set registration id
             */
            $registrationId = Crips::generateUniqueKeyId('ApplReg::');
            $aplContent->add(new AplLine(['Id', $registrationId], 'Registration identity'));
            $configContent->add(new ConfigLine('REG_ID', $registrationId));
            /**
             * Create reset keys
             */
            $resetKeys = ApiSecure::createFingerPrint(15);
            $configContent = self::fingerPrintsToResetKeys($configContent, $resetKeys, self::$BASE_RESET);


            $aplContent->add(new AplLine(['Id', $registrationId], 'Reset key created'));
            /**
             * Execute creation .conf
             */
            $config->create($configContent,  Api::API_REGISTER_APL);
            /**
             * Init application file
             */
            $aplCert = new SaveFile(Api::projectFolder().SupportFiles::REGISTRATION_FILE->folder(), new FileSettings(
                SaveFile::FILE_APL,
                Api::settings()->solutionId,
                Api::settings()->applicationId
            ));
            $aplCert->create($aplContent,  Api::API_CONFIG);
            
            return new ResetKeys($resetKeys);

        }catch(SaveFileError $error)
        {
            $code =  $error->getCode();
            $message =  $error->getMessage();
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, "[E:savefile:{$code}] application registration::{$message}[e]");
        }
        
    }
    
}