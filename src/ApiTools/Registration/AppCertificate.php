<?php

namespace Phr\Webapi\ApiTools\Registration;

use Phr\Webapi\Api;
use Phr\Webapi\ApiBase\RegistratorBase;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiTools\Registration\RegistrationParameters as RP;
use Phr\Webapi\WebApiException;

/**
 * 
 * Create application certificate
 * 
 * @param string encoded parameters
 * 
 * @throws WebApiException
 * 
 */
class AppCertificate extends RegistratorBase
{   
    /**
     * @access public
     * @static
     * @method creates application certificate
     * @return object new reset keys
     */
    public static function create(mixed $_reset_codes, RP|null $RP = null)
    {   
        # Authenticate reset keys !
        self::authenticateResetCodes($_reset_codes->resetKeys);
        
        switch ( $RP ) 
        {
            case RP::NEW_CERT: self::createNewDatabaseCertificate();
                # code...
                break;
            
            default: 
                # code...
                break;
        }

        return false;
    }
    private static function createNewDatabaseCertificate()
    {
        print("create new cert");
    }
}