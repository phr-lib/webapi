<?php

namespace Phr\Webapi\ApiTools\Registration;

use Phr\Webapi\Api;
use Phr\Webapi\ApiTools\Registration\RegistrationParameters as RP;
use Phr\Webapi\ApiBase\ApiVars as APPLICATION;
use Phr\Webapi\ApiBase\RegistratorBase;
use Phr\Webapi\ApiTools\Registration\KeyRegister;
use Phr\Webapi\ApiControl\Contracts\KeyRegisterResponse;
use Phr\Webapi\ApiControl\Contracts\RsaRegistrationResponse;





final class ApplicationRegistrator extends RegistratorBase
{
    public function controller(mixed $_incomeing = null): KeyRegisterResponse|bool|RsaRegistrationResponse
    {   
        switch ( self::$RP ) 
        {
            case RP::NEW_KEY: return KeyRegister::createNewKey(); break;
            case RP::NEW_RSA: return KeyRegister::createNewRsa($_incomeing); break;
            case RP::NEW_CERT: return AppCertificate::create($_incomeing, RP::NEW_CERT); break;
            case RP::NEW_IDP_CERT: return AppCertificate::create($_incomeing, RP::NEW_IDP_CERT); break;    

        }
    }
   
}

