<?php

namespace Phr\Webapi\ApiTools;

use Phr\Webapi\Api;
use Phr\Sqlbridge\Sql;
use Phr\Sqlbridge\SqlSettings;

abstract class DbConnect
{   
    protected static Sql $sql;

    public function __construct( SqlSettings $_settings ) 
    {   
        self::$sql = new Sql($_settings);
    }
}
