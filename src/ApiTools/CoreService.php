<?php

namespace Phr\Webapi\ApiTools;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\Api;
use Phr\Sqlbridge\SqlSettings;
use Phr\Webapi\ApiBase\Support\ApiVars;
use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;


abstract class CoreService extends DbConnect
{       
    public function __construct()
    {           
        parent::__construct($this->appVarDecoderController());
    }
    protected function getSqlSettings(): SqlSettings
    {
        return $this->appVarDecoderController();
    }
    private function appVarDecoderController(): SqlSettings
    {
        if(SHELL::aplExists(SF::REGISTRATION_FILE))
        {   
            $dbSecureKeys = null;
            if(SHELL::aplExists(SF::REGISTRATION_FILE)) $dbKeys = SHELL::retriveDbcert();
            else $dbKeys = null;
            return $this->extractDbSettings($dbKeys, $dbSecureKeys);
        }
        
    }
    private function extractDbSettings(array|null $dbKeys, array|null $dbSecureKeys): SqlSettings
    {   
        $dbCridetials =  ApiSecure::dbKeyCridetials($dbKeys, $dbSecureKeys);
        return(new SqlSettings( SHELL::settings()->dbSettings->host
                                            ,$dbCridetials[0]
                                            ,$dbCridetials[1]
                                            ,SHELL::settings()->dbSettings->database
                                            ,SHELL::settings()->dbSettings->port
                                ));
    }
    
}