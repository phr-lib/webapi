<?php

namespace Phr\Webapi\Settings;

class ApiRoutes 
{   
    public static array $routes;

    public function __construct(array $_routes)
    {   
        self::$routes = $_routes;
    }
}