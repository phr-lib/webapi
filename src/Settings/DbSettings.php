<?php

namespace Phr\Webapi\Settings;

readonly class DbSettings 
{   
    public string $host;
    public int $port;
    public string $database;

    public function __construct(
        string $host,
        int $port,
        string $database
    ){
        $this->host = $host;
        $this->port = $port;
        $this->database = $database;
    }
}