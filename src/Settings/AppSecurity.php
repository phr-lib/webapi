<?php

namespace Phr\Webapi\Settings;

use Phr\Webapi\Settings;

class AppSecurity 
{   
    public string $applicationKey;

    public string $maintainceKey;


    public function __construct(
        string $_application_key,
        string $_maintaince_key

    ){
        $this->applicationKey = $_application_key;
        $this->maintainceKey = $_maintaince_key;

    }
}