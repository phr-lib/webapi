<?php

namespace Phr\Webapi\Settings;

use Phr\Webapi\Settings;

class AppSettings 
{   
    public string $project;

    public string $projectDescripton;

    public string $solutionId;

    public string $applicaiton;

    public string $applicationDescription;

    public string $applicationId;

    public string $appVar;

    public DbSettings $dbSettings;

    public AppSecurity $appSecurity;


    public function __construct(
        string $project,
        string $projectDescripton,
        string $solutionId,
        string $applicaiton,
        string $applicationDescription,
        string $applicationId,
        string $appVar,
        DbSettings $dbSettings,
        AppSecurity $appSecurity


    ){  
        $this->project = $project;
        $this->projectDescripton = $projectDescripton;
        $this->solutionId = $solutionId;
        $this->applicaiton = $applicaiton;
        $this->applicationDescription = $applicationDescription;
        $this->applicationId = $applicationId;
        $this->appVar = $appVar;   
        $this->dbSettings = $dbSettings;
        $this->appSecurity = $appSecurity;
    }
}