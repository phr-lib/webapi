<?php

namespace Phr\Webapi;

use Phr\Webapi\ApiControl\ApiController;

abstract class Controller extends ApiController{}

interface POST 
{
    public function POST(): void;
}
interface GET 
{
    public function GET(): void;
}
interface UPDATE 
{
    public function UPDATE(): void;
}
interface PATCH 
{
    public function PATCH(): void;
}