<?php

namespace Phr\Webapi\Utility;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\ApiTools\CoreService;
use Phr\Sqlbridge\SqlException;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;
use Phr\Certificator\SaveFile;
use Phr\Certificator\SaveFileError;
use Phr\Certificator\FileHandler\ConfigContent;
use Phr\Certificator\FileHandler\Line\ConfigLine;
use Phr\Webapi\ApiBase\Support\FileContent\ConfigMigrations as CONF;
use Phr\Certificator\FileHandler\FileSettings;

class Migrations extends CoreService
{
    
    public function execute()
    {   
        $migrationDate = new \DateTime('now');
        $migrationConfigFile = SHELL::projectFolder().SF::MIGRATION_CONF->file();
        $execudedMigrations = [];
        if(file_exists($migrationConfigFile))
        {
            try{
                $migrationConfig = SaveFile::read($migrationConfigFile);
                foreach($migrationConfig as $key => $value)
                {   
                    if($key == CONF::MIGRATION->value) array_push($execudedMigrations, trim($value));
                }
                
            }catch(SaveFileError $error){}
        }
        $confContent = new ConfigContent(new ConfigLine(CONF::DATE->value, $migrationDate->format(SF::MIGRATION_CONF->dateFormat())));
        foreach(SHELL::migrationFile() as $dir)
        {   
            $migration = SHELL::migrationClass($dir);
            $migrationId = SHELL::migrationId($migration);
            if(in_array($migrationId, $execudedMigrations))
            {
                $confContent->add(new ConfigLine(CONF::MIGRATION->value, $migrationId));
            }
            else
            {   
                try{
                    $migrationClass = 'Migrations\\'.$migration;
                    $migrationEngine = new $migrationClass($this->getSqlSettings());
                    $confContent->add(new ConfigLine(CONF::MIGRATION->value, $migrationId));
                    $migrationEngine->migrate();
                    $confContent->add(new ConfigLine(CONF::MIGRATION->value, $migrationId));
                    
                }catch(SqlException $error){}
                
            }
        }
        try{
            $migrationConfig = new SaveFile(SHELL::projectFolder().SF::MIGRATION_CONF->folder(), new FileSettings(
                SF::MIGRATION_CONF->fileType(),
                SHELL::settings()->solutionId,
                SHELL::settings()->applicationId
            ));
            
            $migrationConfig->create($confContent, SF::MIGRATION_CONF->fileName());
            
            return "Migration completed";

        }catch(SaveFileError $error){}

    }
    
}