<?php

namespace Phr\Webapi\Utility\Setup;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiBase\RegistratorBase;

use Phr\Certificator\Encryption;


class LoadPublic extends RegistratorBase
{
    /// CONSTRUCTOR ***
    public function __construct(string $_title)
    {
        parent::__construct($_title);
        $this->saveApplicationPublicRsa();
    }
    private function saveApplicationPublicRsa()
    {   
        $posted = SHELL::incomeing();

        var_dump($posted);
        exit;
        $key = Encryption::generate32bytsKey();

        
        $encrypt = new Encryption($key);

        $encrypted = $encrypt->fernetEncrypt(json_encode($posted));

        #var_dump($encrypted);
        

    }
    private function saveApplicationPublicRsaOBSELITE()
    {   
        $posted = SHELL::posted();



        #var_dump($posted);
        echo "here I am";
        $rsa = "-----BEGIN PUBLIC KEY-----
        MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAvcSPh9zo9FJU8BS8kokT
        7TYcgDX4OhfE5zRrIEQC4RnY5H/hLIHUMlltB3M07Sgkk03VHP5Vqb/+TOUk3LTz
        /UofEvQu6J4dUo9Tu+1xneePA/c+UFHLUTpjr7d0vpgQ1Zph2JGwdUfL5iOxK/lM
        a1wfZNZReDPXcnoD/vRwCNjm4jFRPleh+1tRFOwVtKqCyKrfOsQQWCWNwTZEn3Bk
        aTZBx3UECdpm5xlDTfLjwBgssYyd+Z0e0I5itXmoYSYwmwf1t64N1lfxfcik3hvQ
        vXPSEvdpI8NEwNSFnEQspATmU/QTJx7KgQlEGMxi8XnFYiuhX0oBHIWYwjuJ2fng
        mwhF5lW32H1UTWz7mJ1fqrg0jvwo50d98JeAGs6vZy0aGU4wwiHRgV5Jf5S8SLEY
        KeONuLDt+w5rN/x8Yw5qyU8Ys/lPIk8/lCPKFex15jYRbhTkmplN6w/XRpcSg77z
        CiopWOcgcA96xV2yiIsVhjxKNffl1pUGikZVAOInHfyZgRWd2Udji8ss6CHyUdUj
        tuvJza81BKo72MKAoX07uwhQJIdJRmGyxbjKgf9jwhsmcRnaSUyH0U/EBL+pvjKp
        L6kx1T8Gu2SIENjP/jqpc7Ebx9DH4tXoFO5SbaLQkZ1fd5YlX673cHX5Z3InLOkR
        fqkE6hTUoFnnqE0vONkTIxsCAwEAAQ==
        -----END PUBLIC KEY-----
        ";

        $key = Encryption::generate32bytsKey();

        $content = Encryption::encodeHex($rsa);

        
        $encrypt = new Encryption($key);

        $encrypted = $encrypt->fernetEncrypt($content);
        #var_dump($encrypted);
        $decrypt = $encrypt->fernetDecrypt($encrypted);

        $contentDe = Encryption::decodeHex($decrypt);
        var_dump($contentDe);

    }
}