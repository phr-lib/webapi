<?php

namespace Phr\Webapi\Utility\Setup;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiBase\RegistratorBase;
use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;
use Phr\Webapi\ApiBase\Support\RegistrationParameters as RP;

use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiControl\Contracts\Setup\KeyRegisterResponse;
use Phr\Webapi\ApiControl\Contracts\Setup\ResetKeys;
use Phr\Certificator\Crips;

use Phr\Certificator\FileHandler\Line\ConfigLine;
use Phr\Certificator\FileHandler\Line\AplLine;

use Phr\Webapi\ApiBase\Support\FileContent\ConfigFile;
/**
 * 
 * Application registration.
 * 
 * 
 * 
 * Executed only once!
 * 
 * 
 * 
 */
class Registration extends RegistratorBase
{   
    protected KeyRegisterResponse $response;

    /// CONSTRUCTOR ***
    public function __construct(string $_title)
    {
        parent::__construct($_title);
        $this->registrationProtocol();
    }
    /**
     * @access public
     * @method response
     * @return KeyRegisterResponse
     */
    public function response(): KeyRegisterResponse
    {
        return $this->response;
    }
    /**
     * 
     * 
     * @access private
     * 
     * 
     */
    private function registrationProtocol(): void
    {   
        if(!file_exists(SHELL::masterKey()))
        {   
            /**
             * syss file must be created.
             */
            if(!is_dir(SHELL::syssFolder()))
                throw new WebApiException(RC::CONFLICT, ERR::E5604005);
            
            $syss = SHELL::syssFolder();
            /**
             * Check or create all 
             * suport folders.
             */
            $regsFolder =  $sysRegs = $syss.SF::REGISTRATION_FILE->folder();
            self::createDir($regsFolder);

            $logsFolder =  $sysRegs = $syss.SF::LOG->folder();
            self::createDir($logsFolder);

            $storageFolder =  $sysRegs = $syss.SF::STORAGE->folder();
            self::createDir($storageFolder);

            $projectFolder =  SHELL::projectFolder();
            self::createDir($projectFolder);

            $sessionFolder = $projectFolder.SF::SESSION->folder();
            self::createDir($sessionFolder);

            $confFolder = $projectFolder.SF::REGISTRATION_CONFIG_FILE->folder();
            self::createDir($confFolder);

            $certFolder = $projectFolder.SF::GLOBAL_PUBLIC_RSA->folder();
            self::createDir($certFolder);
            
            /**
             * Generate registration id.
             */
            $registrationId = Crips::generateUniqueKeyId('Reg_Id::');
            $this->configContent->add(new ConfigLine(ConfigFile::REG_ID->value, $registrationId));
            $this->aplContent->add(new AplLine(['Id:', $registrationId], "Registration id"));
            /**
             * 
             * 
             * Generate new application
             * key !!!
             * 
             * 
             */
            parent::generateNewApplicationKey();
            /**
             * Generate temp database cridentials
             * based on new application key. 
             * 
             * Username and password are generated each
             * call. When registered this user and password
             * must me added to sql database. 
             * 
             */
            $dbCridetials = ApiSecure::dbKeyCridetials();
            /**
             * Create reset fingerprints. Rest keys
             * are used for databse cridentials
             * reset. 
             */
            $fingerPrints = ApiSecure::createFingerPrint(15);
            /**
             * Hash and save hashed fingerprint 
             * into register.conf file ( secret application folder )
             */
            $this->fingerPrintsToResetKeys($fingerPrints, RP::NEW_KEY->algo());
            $this->createConfigFile(SF::REGISTRATION_CONFIG_FILE->fileName());
            /**
             * Create apl file.
             */
            $this->createAplFile(SF::REGISTRATION_FILE->fileName());
            /**
             * Save response database cridentials
             * and reset fingerprints. 
             */
            $this->response =  new KeyRegisterResponse($dbCridetials[0], $dbCridetials[1], new ResetKeys($fingerPrints));

        }
        else throw new WebApiException(RC::CONFLICT, ERR::E5604001);
    }
}
