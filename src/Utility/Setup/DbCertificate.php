<?php

namespace Phr\Webapi\Utility\Setup;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\ApiBase\RegistratorBase;
use Phr\Webapi\ApiBase\Support\RegistrationParameters as RP;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;
use Phr\Webapi\ApiBase\Support\FileContent\ConfigFile;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiControl\ResponseCode as RC;

use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Webapi\ApiTools\Keys\DbKeys;

use Phr\Webapi\ApiControl\Contracts\Setup\KeyRegisterResponse;
use Phr\Webapi\ApiControl\Contracts\Setup\ResetKeys;

use Phr\Certificator\SaveFile;
use Phr\Certificator\SaveFileError;
use Phr\Certificator\FileHandler\FileSettings;
use Phr\Certificator\FileHandler\Line\ConfigLine;
use Phr\Certificator\FileHandler\Line\AplLine;
use Phr\Certificator\Crips;



class DbCertificate extends RegistratorBase
{   
    protected $response;

    /// CONSTRUCTOR ***
    public function __construct(string $_title)
    {
        parent::__construct($_title);
        $this->newDbCertProtocol();
    }
    /**
     * @access public
     * @method response
     * @return KeyRegisterResponse
     */
    public function response()
    {
        return $this->response;
    }
    /**
     * @access private
     * 
     * Create new database certificate.
     * 
     */
    private function newDbCertProtocolo()
    {   
        
        $resetCodes = SHELL::posted();
        $this->response = new ResetKeys([
            $resetCodes->resetKeys->rk1,
            $resetCodes->resetKeys->rk2,
            $resetCodes->resetKeys->rk3,
            $resetCodes->resetKeys->rk4,
            $resetCodes->resetKeys->rk5,
        ]);
    }
    private function newDbCertProtocol()
    {   
        $resetCodes = SHELL::posted();
       
        
        
        self::authenticateResetCodes(SF::REGISTRATION_CONFIG_FILE, $resetCodes, RP::NEW_KEY->algo());
        
        
        /**
         * Generate new db registration id
         */
        $registrationId = Crips::generateUniqueKeyId("DB_reg::");
        $this->configContent->add(new ConfigLine(ConfigFile::DB_REG_ID->value, $registrationId));
        $this->aplContent->add(new AplLine(['Id:', $registrationId ], "Registration id"));
        /**
         * Retrive data from registration.conf
         */
        $config = SHELL::retriveFile(SF::REGISTRATION_CONFIG_FILE);
        $this->configContent->add(new ConfigLine(ConfigFile::REG_ID->value, $config[ConfigFile::REG_ID->value]));
        $this->aplContent->add(new AplLine(['Id:', $config[ConfigFile::REG_ID->value] ], "Registration base - primary registration"));
        /**
         * Generate new keyblock string and 
         * calculate new database username 
         * and password.
         */
        $dbCodes = new DbKeys(Crips::generateKeyBlock());
        $dbCridetials = ApiSecure::dbKeyCridetials($dbCodes->dbKeys);
        /**
         * Create new fingerprints for new 
         * reset codes.
         */
        $fingerPrints = ApiSecure::createFingerPrint(15);
        $this->fingerPrintsToResetKeys($fingerPrints, RP::NEW_KEY->algo());
        /**
         * Create support files.
         */
        $this->createConfigFile(SF::DB_CERT_CONF->fileName());
        $this->createAplFile(SF::DB_CERT_APL->fileName());
        /**
         * Retrive db cert hash and
         * create db certificate. First in array
         * is cert hash, second is filename.
         * 
         */
        $certHash = ApiSecure::dbCertHash();
        try{
            $certificate = new SaveFile(SHELL::projectFolder().SF::DB_CERT->folder(), 
            new FileSettings(   SF::DB_CERT->fileType(),
                                SHELL::settings()->solutionId,
                                SHELL::settings()->applicationId,
                                $certHash[0]
                            )
            );
            $certificate->createCertificate($dbCodes, $certHash[1]);

        }catch(SaveFileError $error)
        {
            throw new WebApiException(RC::CONFLICT, ERR::E5604005, $error->getMessage());
        }
        /**
         * Set response same as key register response, 
         * only new reset codes are generated and 
         * valid for new update or db cridentials
         * reset.
         */
        $this->response =  new KeyRegisterResponse($dbCridetials[0], $dbCridetials[1], new ResetKeys($fingerPrints));

    }
    
}