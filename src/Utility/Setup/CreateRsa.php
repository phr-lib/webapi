<?php

namespace Phr\Webapi\Utility\Setup;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiBase\RegistratorBase;
use Phr\Webapi\ApiBase\Support\SupportFiles;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;
use Phr\Webapi\ApiBase\Support\FileContent\ConfigFile;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiControl\Contracts\Setup\ResetKeys;
use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Webapi\ApiBase\Support\RegistrationParameters as RP;
use Phr\Webapi\ApiControl\Contracts\Setup\MaintainceTokenResponse;

use Phr\Certificator\Encry\RsaKeyGenerator;
use Phr\Certificator\FileHandler\Line\ConfigLine;
use Phr\Certificator\FileHandler\Line\AplLine;
use Phr\Certificator\Crips;
use Phr\Certificator\Encryption;


class CreateRsa extends RegistratorBase
{   
    protected MaintainceTokenResponse $response;

    /// CONSTRUCTOR ***
    public function __construct(string $_title)
    {
        parent::__construct($_title);
        $this->createRsaProtocol();
    }
    /**
     * @access public
     * @method response
     * @return MaintainceTokenResponse
     */
    public function response(): MaintainceTokenResponse
    {
        return $this->response;
    }
    private function createRsaProtocol()
    {   
        if(file_exists( SHELL::projectFolder().SF::GLOBAL_PUBLIC_RSA->file()) )
            throw new WebApiException(RC::CONFLICT, ERR::E5604011);

        if(file_exists( SHELL::projectFolder().SF::GLOBAL_PRIVATE_RSA->file()) )
            throw new WebApiException(RC::CONFLICT, ERR::E5604011);

        if(file_exists( SHELL::syssFolder().SF::RSA_CERT_APL->file() ))
            throw new WebApiException(RC::CONFLICT, ERR::E5604011);

        if(file_exists( SHELL::projectFolder().SF::RSA_CERT_CONFIG->file() ))
            throw new WebApiException(RC::CONFLICT, ERR::E5604011);
            
        /**
         * Create new registration.
         */
        $registrationId = Crips::generateUniqueKeyId("RSA_reg::");
        /**
         * Generate new Rsa key pairs
         */
        $generator = new RsaKeyGenerator;
        $generator->createKeyPairs();
        /**
         * Save it to project folder.
         */
        $generator->saveKeys( SHELL::projectFolder().SF::GLOBAL_PUBLIC_RSA->folder(), SF::GLOBAL_PEM_CONFIG->fileName());
        $publicKey = $generator->publicKey();
        /**
         * 
         * Retrive registration.conf and
         * dbregistration.conf and inprint
         * it into new registration.
         * 
         */
        $config = SHELL::retriveFile(SF::REGISTRATION_CONFIG_FILE);
        $dbCofig = SHELL::retriveFile(SF::DB_CERT_CONF);

        $this->configContent->add(new ConfigLine(ConfigFile::RSA_REG_ID->value, $registrationId));
        $this->aplContent->add(new AplLine(['Id:', $registrationId], "Rsa registration succesfull!"));

        $this->configContent->add(new ConfigLine(ConfigFile::REG_ID->value, $config[ConfigFile::REG_ID->value]));
        $this->aplContent->add(new AplLine(['Primary id:', $config[ConfigFile::REG_ID->value] ], "Primary registration"));

        $this->configContent->add(new ConfigLine(ConfigFile::DB_REG_ID->value, $dbCofig[ConfigFile::DB_REG_ID->value]));
        $this->aplContent->add(new AplLine(['Database id:', $dbCofig[ConfigFile::DB_REG_ID->value] ], "Based on primary registration"));
        /**
         * Create new config file.
         */
        $this->createConfigFile(SF::RSA_CERT_CONFIG->fileName());
        /**
         * Create new fingerprints
         */
        $fingerPrints = ApiSecure::createFingerPrint(15);
        $this->fingerPrintsToResetKeys($fingerPrints, RP::MAINTAINCE->algo());
        $resetKeys =  new ResetKeys($fingerPrints);
        /**
         * Create token based od reset
         * keys.
         * 
         */
        $encryptor = Encryption::generate32bytsKey();
        $encrypt = new Encryption($encryptor);
        $token = $encrypt->sslEncrypt(json_encode($resetKeys));
        /**
         * Create signature base od 
         * reset keys enrypted content.
         */
        $singn = Encryption::sslPublicEncrypt($publicKey, $encryptor);
        /**
         * Create apl file and respond
         * maintaince token to sysAdmin
         * (pHr console)
         */
        $this->createAplFile(SF::RSA_CERT_APL->fileName());
        $this->response = new MaintainceTokenResponse($singn,$token);

    }
    
}