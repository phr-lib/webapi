<?php

namespace Phr\Webapi\Utility\Entity;

use Phr\Sqlbridge\Entity;

class Sessions extends Entity
{
    public string $userId;
    public string $sessionId;
    public int $sessionTs;
    public int $expire;
    public string $enryptor;
    public int $sessionIv;
    public string $token;


    public function __construct(
        string $userId,
        string $sessionId,
        int $sessionTs,
        int $expire,
        string $enryptor,
        int $sessionIv,
        string $token
    ){
        $this->userId = $userId;
        $this->sessionId = $sessionId;
        $this->sessionTs = $sessionTs;
        $this->expire = $expire;
        $this->enryptor = $enryptor;
        $this->sessionIv = $sessionIv;
        $this->token = $token;
    }
    public static function entity(array $data): self 
    {
        return new self(
            $data['userId'],
            $data['sessionId'],
            $data['sessionTs'],
            $data['expire'],
            $data['enryptor'],
            $data['sessionIv'],
            $data['token']
        );
    }

}