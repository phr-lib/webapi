<?php

namespace Phr\Webapi\Utility\Entity;

use Phr\Sqlbridge\Entity;

class Logs extends Entity
{   
    public int $day;
    public int $month;
    public int $year;
    public int $ts;
    public string $sessionId;
    public string $userId;
    public string $event;
    public string $text;


    public function __construct(
        int $day,
        int $month,
        int $year,
        int $ts,
        string $sessionId,
        string $userId,
        string $event,
        string $text
    ){
        $this->day = $day;
        $this->month = $month;
        $this->year = $year;
        $this->ts = $ts;
        $this->sessionId = $sessionId;
        $this->userId = $userId;
        $this->event = $event;
        $this->text = $text;
    }

}