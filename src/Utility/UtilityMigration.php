<?php

namespace Phr\Webapi\Utility;

use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\ApiShell;
use Phr\Sqlbridge\SqlSettings;
use Phr\Sqlbridge\Migrations\Engine;
use Phr\Sqlbridge\Migrations\Type;
use Phr\Webapi\ApiTools\CoreService;
use Phr\Webapi\Utility\Entity\Sessions;
use Phr\Webapi\Utility\Entity\Logs;
use Phr\Certificator\SaveFile;
use Phr\Certificator\FileHandler\FileSettings;
use Phr\Certificator\SaveFileError;
use Phr\Certificator\FileHandler\Line\AplLine;
use Phr\Certificator\FileHandler\AplContent;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;
use Phr\Sqlbridge\SqlException;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiBase\Errors as ERR;



class SessionMigration extends Engine
{
    public function migrate()
    {   
        $array = [
            [
                new Sessions(self::VARCHAR, self::VARCHAR, self::INTIGER, self::INTIGER, self::VARCHAR, self::INTIGER, self::VARCHAR), 
                [[100, false, Type::PRIMARY->sql()],[100, false, Type::UNIQUE->sql()],[50],[50],[100, false],[50],[5000, false]]
            ],
            [
                new Logs(self::INTIGER, self::INTIGER, self::INTIGER, self::INTIGER, self::VARCHAR, self::VARCHAR, self::VARCHAR, self::VARCHAR), 
                [[50],[50],[50],[50],[100],[50, false],[100, null],[500, null]]
            ]
        ];#@end parser ***

        $this->parser($array);
    }
}

class UtilityMigration extends CoreService
{
    public function execute()
    {   
        try
        {
            $migration = new SessionMigration($this->getSqlSettings());
            $migration->migrate();
        }catch(SqlException $error)
        {
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604010, $error->getMessage());
        }
        
        try 
        {
            $setupAplFile = new SaveFile(ApiShell::syssFolder().SF::SETUP_APL->folder(), 
            new FileSettings(   SF::SETUP_APL->fileType()
                                ,ApiShell::settings()->solutionId
                                ,ApiShell::settings()->applicationId
                                ,null
                            )
            );
            $content = new AplContent("BASIC SETUP", new AplLine(['session', "created"], 'Basic application databases'));
            $content->add(new AplLine(['logs', "created"], 'Application log db'));
            $setupAplFile->create($content, SF::SETUP_APL->fileName());

        }catch(SaveFileError $error)
        {
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5100300, $error->getMessage());
        }
    }
}