<?php

namespace Phr\Webapi\Utility;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\Utility\UtilityMigration;
use Phr\Webapi\Utility\Migrations;

use Phr\Webapi\ApiControl\Contracts\Secure\ClientCertResponse;

use Phr\Webapi\ApiControl\Response;
use Phr\Webapi\ApiBase\MaintainceBase;
use Phr\Webapi\ApiBase\Support\ProcessHeaderTypes as HT;
use Phr\Webapi\ApiBase\Support\RegistrationParameters as RP;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;

use Phr\Webapi\Utility\Setup\Registration;
use Phr\Webapi\Utility\Setup\DbCertificate;
use Phr\Webapi\Utility\Setup\CreateRsa;
use Phr\Webapi\ApiControl\Authorization\Sessions\SessionControl;

use Phr\Webapi\ApiControl\Contracts\Maintaince\DiagnozeResponse;
use Phr\Webapi\ApiControl\Contracts\Maintaince\RegistrationInfo;
use Phr\Webapi\ApiControl\Authorization\Session;
/**
 * 
 * @final maintaince
 * 
 * 
 */
final class Maintaince extends MaintainceBase
{   
    private bool $masterKey = false;

    public function __construct()
    {   
        parent::__construct(SHELL::parameters());
    }
    public function controller()
    {   
        #self::checkHeader($_header);
        
        switch ( SHELL::method() ) 
        {
            case SHELL::GET:   switch ( SHELL::route() ) 
                                {   
                                    case 'migrations': $migrations = new Migrations;Response::Accepted($migrations->execute()); break;
                                    case 'client-cert': Response::Ok(new ClientCertResponse); break;
                                    case 'setup':  $migrations = new UtilityMigration; $migrations->execute(); Response::Accepted('App setup complete'); break;
                                    case 'test': SessionControl::createNewCertSession('userId'); break;
                                    default: Response::Ok($this->diagnoze()); break;
                                }
            break;

            case SHELL::PATCH: 
                                switch ( parent::$RP ) 
                                {
                                        case RP::NEW_KEY:   $registration = new Registration("REGISTRATION");
                                                            Response::Ok($registration->response());
                                            break;
                                      
                                }
            break;

            case SHELL::POST: 
                                switch ( parent::$RP ) 
                                {
                                        case RP::NEW_KEY:   $dbCertificate = new DbCertificate("DB CERTIFICATE");
                                                            Response::Ok($dbCertificate->response());
                                            break;

                                        case RP::NEW_RSA:   $dbCertificate = new CreateRsa("CREATE RSA");
                                                            Response::Ok($dbCertificate->response());
                                            break;
                                    
                                }
            break;
            
            default: throw new WebApiException(RC::METHOD_NOT_ALLOWED); break;
        }
        exit;
    }
    private static function checkHeader(string $_header): true
    {   
        $brakeHeader = explode(HT::HEADER_DELIMITER->code(), $_header);

        if(isset($brakeHeader[1]) == SHELL::applicationKey()) return true;
        else throw new WebApiException(RC::EXPECTATION_FAILED, ERR::E5605101);
    }
    private function diagnoze(): DiagnozeResponse
    {   
        $registrationFile = false;

        $dbRegistrationFile = false;

        $rsaFile = false;

        $appSetup = false;

        $master = false;

        if(file_exists(SHELL::masterKey())) $this->masterKey = true;
         
        if(file_exists(SHELL::syssFolder().SF::REGISTRATION_FILE->file())) $registrationFile = true;

        if(file_exists(SHELL::syssFolder().SF::DB_CERT_APL->file())) $dbRegistrationFile = true;

        if(file_exists(SHELL::syssFolder().SF::RSA_CERT_APL->file())) $rsaFile = true;

        if(file_exists(SHELL::syssFolder().SF::SETUP_APL->file())) $appSetup = true;


        return new DiagnozeResponse(
            $this->masterKey,
            $registrationFile,
            $dbRegistrationFile,
            $rsaFile,
            $appSetup,
            $master
        );
    }
}
