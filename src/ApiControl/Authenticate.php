<?php

namespace Phr\Webapi\ApiControl;

use Phr\Webapi\ApiBase\ApiAuthorization;
use Phr\Webapi\ApiBase\ActiveAuthorization as ACTIVEAUTH;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\WebApiException;


/**
 * @see code 56 05 ***
 */
class Authenticate extends ApiAuthorization
{   
    public static function timecall()
    {
        if(ACTIVEAUTH::AUTHTIME == parent::$ACTIVE)
            throw new WebApiException(RC::EXPECTATION_FAILED, ERR::E5605001);
    }
    public static function application(): true
    {
        if(ACTIVEAUTH::APPLICATION == parent::$ACTIVE) return true;
        else throw new WebApiException(RC::EXPECTATION_FAILED, ERR::E5605001);      
    }
    public static function authorisationLevel(): ACTIVEAUTH
    {
        return self::$ACTIVE;
    }
   
}