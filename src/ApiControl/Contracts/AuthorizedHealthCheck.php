<?php

namespace Phr\Webapi\ApiControl\Contracts;

use Phr\Webapi\Api;
use Phr\Shell\ShellBase\HcResponse;


readonly class AuthorizedHealthCheck extends HcResponse
{   
    public string $webApiVersion;

    public string $project;

    public string $applicationType;


    public function __construct()
    {   
        parent::__construct();
        $this->webApiVersion = Api::VERSION;
        $settings = Api::settings();
        $this->project = $settings->project;
        $this->applicationType = $settings->appVar;

    }
}