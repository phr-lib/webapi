<?php

namespace Phr\Webapi\ApiControl\Contracts;

readonly class ListResponse 
{
    public array $dataList;

    public function __construct(array $dataList)
    {
        $this->dataList = $dataList;
    }
}