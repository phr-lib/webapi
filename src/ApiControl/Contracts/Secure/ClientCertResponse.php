<?php

namespace Phr\Webapi\ApiControl\Contracts\Secure;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\ApiTools\ApiSecure;

readonly class ClientCertResponse 
{   
    public string $serverId; 

    public string $publicPem;

    public string $serverIp;

    public function __construct()
    {
        $this->serverId = SHELL::settings()->applicationId;
        $this->publicPem = ApiSecure::publicRsaHex();
        $this->serverIp = $_SERVER['SERVER_ADDR'];
    }
}