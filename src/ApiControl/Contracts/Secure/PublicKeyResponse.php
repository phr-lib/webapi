<?php

namespace Phr\Webapi\ApiControl\Contracts\Secure;


readonly class PublicKeyResponse
{   
    public string $publicKey;

    public function __construct(
        string $publicKey
    ){
        $this->publicKey = $publicKey;
    }
}