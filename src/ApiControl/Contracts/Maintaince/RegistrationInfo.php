<?php

namespace Phr\Webapi\ApiControl\Contracts\Maintaince;

use Phr\Webapi\Api;
use Phr\Webapi\WebApiException;

use Phr\Certificator\SaveFile;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;
use Phr\Certificator\SaveFileError;

use Phr\Webapi\ApiBase\Support\FileContent\ConfigFile;

readonly class RegistrationInfo 
{   
    public string $registrationDate;

    public string $registrationId;


    public function __construct()
    {
        $configFile = Api::projectFolder().SF::REGISTRATION_CONFIG_FILE->file();

        try{ if(file_exists($configFile)) $content = SaveFile::read($configFile);
        }catch(SaveFileError $error)
        { 
            throw new WebApiException(RC::CONFLICT, ERR::E5604005, $error->getMessage()); 
        }    
        $this->registrationDate = $content[ConfigFile::DATE->value];
        $this->registrationId = $content[ConfigFile::REG_ID->value];

    }
}