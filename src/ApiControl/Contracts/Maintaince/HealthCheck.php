<?php

namespace Phr\Webapi\ApiControl\Contracts\Maintaince;

use Phr\Webapi\ApiBase\ApiShell as SHELL;

readonly class HealthCheck
{   
    public string $project;

    public string $uptime;


    public function __construct()
    {   
        $settings = SHELL::settings();
        $this->project = $settings->project;
        $this->uptime = 'OK';
    }
}
