<?php

namespace Phr\Webapi\ApiControl\Contracts\Maintaince;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;


readonly class DiagnozeResponse 
{   
    public string $applicationKey;

    public string $registrationFile;

    public string $dbRegistrationFile;

    public string $rsaFile;

    public string $appSetup;

    public function __construct(){

        $this->applicationKey = $this->set(file_exists(SHELL::masterKey()));
        $this->registrationFile = $this->set(file_exists(SHELL::syssFolder().SF::REGISTRATION_FILE->file()));
        $this->dbRegistrationFile = $this->set(file_exists(SHELL::syssFolder().SF::DB_CERT_APL->file()));
        $this->rsaFile = $this->set(file_exists(SHELL::syssFolder().SF::RSA_CERT_APL->file()));
        $this->appSetup = $this->set(file_exists(SHELL::syssFolder().SF::SETUP_APL->file()));
    }
    protected function set(bool $_status)
    {
        return $_status ? 'OK':'NOT SET';
    }
}