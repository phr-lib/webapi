<?php

namespace Phr\Webapi\ApiControl\Contracts\Setup;

readonly class ResetKeys
{   
    public string $rk1;

    public string $rk2;

    public string $rk3;

    public string $rk4;

    public string $rk5;

    
    public function __construct(array $_reset_keys)
    {
        $this->rk1 = $_reset_keys[0];
        $this->rk2 = $_reset_keys[1];
        $this->rk3 = $_reset_keys[2];
        $this->rk4 = $_reset_keys[3];
        $this->rk5 = $_reset_keys[4];
    }
}