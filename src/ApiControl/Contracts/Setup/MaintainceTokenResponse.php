<?php

namespace Phr\Webapi\ApiControl\Contracts\Setup;


readonly class MaintainceTokenResponse
{   
    public string $signature;

    public string $maintainceToken;

    public function __construct(
        string $signature,
        string $maintainceToken
    ){
        $this->signature = $signature;
        $this->maintainceToken = $maintainceToken;
    }
}