<?php

namespace Phr\Webapi\ApiControl\Contracts\Setup;

use Phr\Webapi\ApiControl\Contracts\Setup\ResetKeys;

readonly class KeyRegisterResponse
{   
    public string $dbu;

    public string $dbp;

    public ResetKeys $resetKeys;


    public function __construct(
        string $dbu
        ,string $dbp
        ,ResetKeys $resetKeys
    ){
        $this->dbu = $dbu;
        $this->dbp = $dbp;
        $this->resetKeys = $resetKeys;
    }
}