<?php

namespace Phr\Webapi\ApiControl\Contracts;

use Phr\Webapi\ApiTools\Keys\ResetKeys;

readonly class RsaRegistrationResponse
{   
    public string $certificate;

    public KeyRegisterResponse $keyRegistration;


    public function __construct(
        string $_certificate
        ,KeyRegisterResponse $keyRegistration
    ){  
        $this->certificate = $_certificate;
        $this->keyRegistration = $keyRegistration;
    }
}