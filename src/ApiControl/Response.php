<?php

namespace Phr\Webapi\ApiControl;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\ApiControl\ResponseCode as RC;

final class Response
{
    public static function Ok(object|null $_response = null): void
    {
        SHELL::response(RC::OK, $_response);
    }
    public static function Created(object|string|null $_response = null): void
    {
        SHELL::response(RC::CREATED, $_response);
    }
    public static function Accepted(object|string|null $_response = null): void
    {
        SHELL::response(RC::ACCEPTED, $_response);
    }
    public static function NoContent(object|string|null $_response = null): void
    {
        SHELL::response(RC::NOCONTENT, $_response);
    }
    public static function BadRewuest(object|string|null $_response = null): void
    {
        SHELL::response(RC::BADREQUEST, $_response);
    }
    public static function UnAuthorized(object|string|null $_response = null): void
    {
        SHELL::response(RC::UNAUTHORIZED, $_response);
    }
    public static function Forbidden(object|string|null $_response = null): void
    {
        SHELL::response(RC::FORBIDDEN, $_response);
    }
    public static function Conflict(object|string|null $_response = null): void
    {
        SHELL::response(RC::CONFLICT, $_response);
    }
    public static function MethodNotAllowed(): void
    {
        SHELL::response(RC::METHOD_NOT_ALLOWED);
    }
    public static function Gone(): void
    {
        SHELL::response(RC::GONE);
    }
    public static function EncryptedOk(object $_content): void
    {
        SHELL::secureResponse($_content);
    }
}