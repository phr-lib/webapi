<?php

namespace Phr\Webapi\ApiControl;

use Phr\Webapi\Api;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Shell\Shell as SHELL;
use Phr\Webapi\ApiBase\ApiShell;
use Phr\Shell\Http\IHttpMethod;
use Phr\Shell\Http\ResponseCode;
use Phr\Webapi\IApi;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiTools\RegisterApp;
use Phr\Eojwt\Accounts\UserRols;

#use Phr\Webapi\ApiBase\WebapiBase;
#use Phr\Shell\ShellBase\Authorization;



/**
 * @abstract class modedel for api
 * controllers.
 * 
 * @see code 56 00 000
 * @throws 001
 */

abstract class ApiController implements IHttpMethod, ResponseCode
{   
    protected static string $method;

    protected static $posted;

    /**
     * @abstract
     * @access public
     * @method controller
     */
    abstract public function controller(): void;
    
    /**
     * @method respond via shell response. 
     * Json format
     */
    protected static function response(RC $_response_code, object|string $_content = null)
    {   
        if($_response_code == RC::ACCEPTED)
        {
            SHELL::setResponseHeader($_content);
            $content = null;
            
        }else $content = $_content;
            
        SHELL::response($_response_code->value, $content);
    }
    /**
     * @method gets Encry-token header and 
     * authenticate with key in appconfig!
     * @return bool if authenticated!
     * @throws WebApiException
     */
    protected static function authenticateApplication(): bool
    {  
        $applicationKey =  SHELL::getHeaderByKey(IApi::API_KEY_HEADER);
        if(!$applicationKey) throw new WebApiException(self::NOTACCEPTABLE);
        if(Api::settings()->security->appKey !== $applicationKey)
            throw new WebApiException(self::UNAUTHORIZED);
        return true;
    }
    /**
     * @method gets posted data from 
     * php :://incomeing - REST POST, UPDATE METHOD
     * For PATCH user parameters - smaller faster updates
     * 
     */
    protected static function posted()
    {
        return SHELL::posted();
    }
    protected static function method()
    {
        return SHELL::method();
    }
    protected static function parameters(): string|null
    {   
        $params = SHELL::routes();        
        $par = explode('/', $params);
        return array_pop($par);
    }
    protected static function registerApp(object $_posted_keys)
    {
        new RegisterApp($_posted_keys);
    }
    public static function authenticate(UserRols|null $_rols = null)
    {   
        
        ApiShell::sessionControl();
    }
    
}
