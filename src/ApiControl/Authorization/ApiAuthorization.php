<?php

namespace Phr\Webapi\ApiControl\Authorization;

use Phr\Shell\Shell;
use Phr\Shell\IShell;
use Phr\Shell\Http\ResponseCode;
use Phr\Shell\ShellBase\Authorization;

use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiBase\Support\ProcessHeaderTypes as HT;
use Phr\Webapi\ApiBase\Support\ActiveAuthorization as ACTIVEAUTH;
use Phr\Webapi\Utility\Maintaince;
use Phr\Webapi\Settings\AppSecurity;


abstract class ApiAuthorization
{   
    protected static string $head;

    protected static string $key;

}