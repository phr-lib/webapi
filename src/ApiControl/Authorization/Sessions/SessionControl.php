<?php

namespace Phr\Webapi\ApiControl\Authorization\Sessions;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Eojwt\Accounts\ServerAccount;
use Phr\Eojwt\Accounts\SessionAccount;
use Phr\Eojwt\Accounts\SessionFp;
use Phr\Eojwt\EoJwt;
use Phr\Eojwt\EoJwtException;
use Phr\Webapi\ApiControl\Authorization\Sessions\WebSession\SessionBase;
use Phr\Webapi\ApiControl\Authorization\Sessions\WebSession\CertSession;
use Phr\Webapi\ApiControl\Authorization\Sessions\WebSession\ActiveSession;
use Phr\Webapi\ApiControl\Authorization\Sessions\WebSession\DbSession;
use Phr\Eojwt\Tokens\ActiveJwt;
use Phr\Eojwt\Accounts\ActiveToken;
use Phr\Webapi\ApiControl\Authorization\Sessions\WebSession\ActiveUser;


class SessionControl extends ActiveSession
{   
    public function  __construct()
    {   
        $token = SHELL::authorization();
        $privateRsa = ApiSecure::retrivePrivateRsa();
        
        
        try{
            $jwt = new EoJwt($token);
            $jwt->validate($privateRsa, false);
            $account = $jwt->payload()->account;
            $sessionAccount = $account->sessionAccount;
            
            $this->serverAccount = new ServerAccount(
                $account->serverId,
                new SessionAccount(
                    $sessionAccount->userId,
                    $sessionAccount->sessionId,
                    $sessionAccount->sessionTs,
                    $sessionAccount->expire,
                    $sessionAccount->enryptor,
                    $sessionAccount->sessionIv,
                    new SessionFp([
                        $sessionAccount->sessionFingerprints->fp1,
                        $sessionAccount->sessionFingerprints->fp2,
                        $sessionAccount->sessionFingerprints->fp3,
                        $sessionAccount->sessionFingerprints->fp4,
                        $sessionAccount->sessionFingerprints->fp5
                    ])
                ),
                $account->clientIp
            );
            
            $dbSession = new DbSession($this->serverAccount);
            $activeToken = new ActiveJwt;
            $activeToken->create($this->serverAccount);
            $dbSession->createSession($activeToken->encode());


        }catch(EoJwtException $error){
            if(5393501 == $error->getCode())
            throw new WebApiException(RC::UNAUTHORIZED, ERR::E5655001, $error->getMessage());
            else throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604010, $error->getMessage());
        }
        
    }
    public static function activeSessionControl(ActiveToken $token): ActiveUser
    {   
        $session = DbSession::findSession($token->sessionId);
        
        if(!$session) new WebApiException(RC::UNAUTHORIZED, ERR::E5655001, 'no session');
        if($session->token === $token->token)
        {   
            return(new ActiveUser(
                $session->userId,
                $session->sessionId,
                $session->enryptor,
                $session->sessionIv
            ));
        }else new WebApiException(RC::UNAUTHORIZED, ERR::E5655001, 'token');
    }
    
}