<?php

namespace Phr\Webapi\ApiControl\Authorization\Sessions\WebSession;

use Phr\Webapi\Service;
use Phr\Webapi\Utility\Entity\Sessions;
use Phr\Certificator\Encryption;
use Phr\Sqlbridge\SqlException;

class DbSession extends ActiveSession
{   
    public function createSession(string $token)
    {   
        try
        {
        
            $service = new Service;

            $exists = $service->fetch(Sessions::class, ['userId', $this->serverAccount->sessionAccount->userId]);
            if($exists)
            {   
                $service->update(new Sessions(
                    $this->serverAccount->sessionAccount->userId,
                    $this->serverAccount->sessionAccount->sessionId,
                    $this->serverAccount->sessionAccount->sessionTs,
                    $this->serverAccount->sessionAccount->expire,
                    $this->serverAccount->sessionAccount->enryptor,
                    2135435,
                    $token,
                ), ['userId', $this->serverAccount->sessionAccount->userId]);
            }else
            {   
                $service->insert(new Sessions(
                    $this->serverAccount->sessionAccount->userId,
                    $this->serverAccount->sessionAccount->sessionId, 
                    (int)$this->serverAccount->sessionAccount->sessionTs,
                    (int)$this->serverAccount->sessionAccount->expire,
                    $this->serverAccount->sessionAccount->enryptor,
                    2135435,
                    $token
                ));
            }
        }catch(SqlException $error)
        {
            var_dump($error->getMessage());
        }
        
    }
    public static function findSession(string $sessionId): Sessions
    {   
        $service = new Service;
        $session = $service->fetch(Sessions::class, ['sessionId', $sessionId]);
        if($session == null) return null;
        return Sessions::entity($session[0]);
    }
}