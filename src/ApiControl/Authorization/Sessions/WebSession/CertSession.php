<?php

namespace Phr\Webapi\ApiControl\Authorization\Sessions\WebSession;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\Service;
use Phr\Certificator\SaveFile;
use Phr\Certificator\FileHandler\FileSettings;
use Phr\Certificator\SaveFileError;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;

class CertSession extends SessionBase
{
    public function createSession(string $_key): void
    {   
        try 
        {
            $sessionCert = new SaveFile(SHELL::projectFolder().SF::SESSION_CERT->folder(), 
            new FileSettings(   SF::SESSION_CERT->fileType()
                                ,SHELL::settings()->solutionId
                                ,SHELL::settings()->applicationId
                                ,$_key
                            )
            );
            
            $sessionCert->createCertificate($this, $this->userId);

        }catch(SaveFileError $error)
        {

        }
    }
}