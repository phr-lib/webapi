<?php

namespace Phr\Webapi\ApiControl\Authorization\Sessions\WebSession;

use Phr\Eojwt\Accounts\ServerAccount;


use Phr\Certificator\Crips;
use Phr\Certificator\Encryption;
use Phr\Eojwt\Accounts\SessionFp;
use Phr\Eojwt\Accounts\SessionAccount;


abstract class SessionBase 
{   

    public ServerAccount $serverAccount;
    
    public function __construct(
        SessionAccount $_server_account,
    ){   
        $this->serverAccount = $_server_account->userId;
    }
    protected function fpToken(): string
    {
        return Encryption::baseEncode(json_encode($this->fingerprints));
    }
}