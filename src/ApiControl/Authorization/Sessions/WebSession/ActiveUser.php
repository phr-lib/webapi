<?php

namespace Phr\Webapi\ApiControl\Authorization\Sessions\WebSession;

class ActiveUser 
{
    public string $userId;

    public string $sessionId;

    public string $enryptor;

    public string $sessionIv;

    public function __construct(
        string $userId,
        string $sessionId,
        string $enryptor,
        string $sessionIv
    ){
        $this->userId = $userId;
        $this->sessionId = $sessionId;
        $this->enryptor = $enryptor;
        $this->sessionIv = $sessionIv;
    }

}