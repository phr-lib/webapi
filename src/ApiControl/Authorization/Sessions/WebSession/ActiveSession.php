<?php

namespace Phr\Webapi\ApiControl\Authorization\Sessions\WebSession;

use Phr\Certificator\Crips;
use Phr\Certificator\Encryption;
use Phr\Eojwt\Accounts\ServerAccount;
use Phr\Eojwt\Accounts\SessionAccount;

abstract class ActiveSession 
{   
    protected ServerAccount $serverAccount;
 
    public function __construct(     
        ServerAccount $_server_account
    ){   
        $this->serverAccount = $_server_account;      
    }
}