<?php

namespace Phr\Webapi\ApiControl\Authorization;

use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiBase\Support\ProcessHeaderTypes as PHT;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\Utility\Maintaince;
use Phr\Webapi\ApiControl\Contracts\Secure\PublicKeyResponse;
use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Webapi\ApiControl\Authorization\Sessions\SessionControl;

class HeaderAuthorization
{   
    private static string $head;

    private static string $key;

    public static function getHead(): string { return self::$head; }

    public static function getKey(): string { return self::$key; }

    public static function controller()
    {  
        if(SHELL::process())
        {
            $header = SHELL::process();
            if(!preg_match('/'.PHT::HEADER_DELIMITER->code().'/', $header))
                throw new WebApiException(RC::EXPECTATION_FAILED, ERR::E5605000);
            
            $headerParts = explode(PHT::HEADER_DELIMITER->code(), $header);
            if(isset($headerParts[0])) $head = $headerParts[0];
            if(isset($headerParts[1])) self::$key = $headerParts[1];
            if(substr($header,0,1)  !== '#') throw new WebApiException(RC::EXPECTATION_FAILED, ERR::E5605000);
            self::$head = substr($header,1,3);
            self::headController();
        }
    }
    
    private static function headController()
    {   
        $timeHeader = substr(self::$head, 0, 1);
        if(substr(self::$head, 0, 1) === '%') self::checkTimeHeader();
        if(substr(self::$head, 1, 1) === '$') self::checkEncryHeader();
        if(substr(self::$head, 1, 1) === '?') self::checkMaintainceHeader();
        if(substr(self::$head, 0, 2) === '%?') self::applicationMaintaince();
       
    }
    private static function checkTimeHeader()
    {   
        if(substr(self::$head, 1, 1) === '%') $key = md5(substr(time(),0,8));
        else $key = md5(substr(time(),0,8).SHELL::applicationKey());
        #if(self::$key !== $key) throw new WebApiException(RC::NOT_ACCEPTABLE, ERR::E5605012);
    }
    private static function checkEncryHeader()
    {
        if(substr(self::$head, 0, 3) === '%$?')
        {
            $session = new SessionControl;
            SHELL::response(RC::ACCEPTED, "SESSION");
            exit;
        }
        elseif(substr(self::$head, 0, 3) === '%$$')
        {   
            $result = ApiSecure::digestEncry();
        }
    }
    private static function checkMaintainceHeader()
    {   
        if(!SHELL::authorization())
                throw new WebApiException(RC::NOT_ACCEPTABLE, ERR::E5605012);
        if(SHELL::settings()->appSecurity->maintainceKey !== SHELL::authorization())
                throw new WebApiException(RC::NOT_ACCEPTABLE, ERR::E5605012);

        if(substr(self::$head, 0, 3) === '%?%'){
            
        }
        elseif(substr(self::$head, 0, 3) === '%?$'){
            
            ApiSecure::digestEncry();
        }
    }
    private static function applicationMaintaince()
    {   
        $maintaince = new Maintaince;
        $maintaince->controller();
    }
    
}