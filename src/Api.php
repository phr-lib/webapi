<?php

namespace Phr\Webapi;

use Phr\Webapi\ApiBase\ApiShell;
use Phr\Webapi\ApiControl\Response;

use Phr\Shell\Shell;
use Phr\Shell\Http\ResponseCode;

use Phr\Webapi\Settings\ApiRoutes;
use Phr\Webapi\Settings\AppSettings;

use Phr\Webapi\ApiBase\Errors;
use Phr\Webapi\ApiBase\ActiveAuthorization as ACTIVEAUTH;

use Phr\Webapi\ApiControl\Authenticate;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiControl\Contracts\Maintaince\HealthCheck;

use Phr\Webapi\ApiControl\Contracts\AuthorizedHealthCheck;
use Phr\Webapi\ApiControl\Contracts\KeyRegisterResponse;
use Phr\Webapi\ApiTools\Registration\ApplicationRegistrator;



class Api extends ApiBase\WebApiBase implements IApi
{   
    

    /// CONSTRUCTOR ***

    public function __construct( string $_ROOT )
    {   
        parent::__construct($_ROOT);

        require_once 'loaders/loadmigrations.php';
        
        $this->loadAppConfig();

        $this->calculateApplicationFolders();
    }
    
    public function run(string $controller): void 
    {   
        $this->pathController();
        $controller = new $controller;
        $controller->controller();
        Response::MethodNotAllowed();
    }
    public static function apiError( WebApiException $exception )
    {   
        Shell::errorResponse( $exception );
    }

    public static function healthCheck()
    {   
        ApiShell::response(RC::OK, new HealthCheck);
    }

    
    public function routes(ApiRoutes $_set_api_routes)
    {   
        self::$apiRoutes =  $_set_api_routes;
    }
    public static function authenticate(array $_rols): array
    {
        return ["authenticate"=> ["jwt" => true, "rols" => $_rols]];
    }
    
    
    
    

   
    
    /*
    public static function apiRoutes(array|null $rools = null)
    {
        return self::$apiRoutes::$routes;
    }
    public function configure( string $_path_to_config ): void
    {   
        try
        {   
            if(!file_exists( self::$ROOT . $_path_to_config )) throw new SettingsException("missing setting file");
            $Configuration = new SettingsConfig( self::$ROOT, $_path_to_config );
            
            self::$appSettings = $Configuration->get();
            
        }
        catch( SettingsException $error)
        {
            throw new WebApiException( $error->getMessage() );
        }     
    }
    
    public static function createLockFile( string $_config_file_path )
    {   
        try
        {   
            $Settings = json_decode( file_get_contents( self::$ROOT . $_config_file_path ) );

            $SettingsLock = new SettingsLock( self::$ROOT, $Settings );

            $SettingsLock->createFile( self::$ROOT );
        }
        catch( SettingsException $error)
        {
            throw new WebApiException( $error->getMessage() );
            
        }
    }

    /**
     * @method useControllers
     *
    public function useControllers(): void
    {
        ApiController::reroute();
    }

    public static function getAppSettings():AppSettings
    {
        return parent::$appSettings;
    }
    
    
    
    

    /**
     * @access private 
     * 
     * @method includeAutoloader
     *
    private static function includeAutoloader()
    {
        require_once 'loaders/load.php';
    }
    */

}