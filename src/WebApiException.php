<?php

namespace Phr\Webapi;

use Phr\Webapi\ApiControl\ResponseCode;
use Phr\Webapi\ApiBase\Errors;

class WebApiException extends \Exception
{
    public function __construct(ResponseCode $_response_code, Errors|null $_code_message = null, string $_message = "")
    {
        $this->code = $_response_code->value;
        #$this->message = $_code_message->code().' '.$_message;
        $this->message = $_code_message->message().' '.$_message;

    }
}